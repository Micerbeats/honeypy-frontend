export const environment = {
  production: false,
  auth: false,
  url: "http://localhost:4200/",
  testEnv: "http://127.0.0.1:30001",
  reportEnv: "http://127.0.0.1:30002",
  hostEnv: "http://127.0.0.1:30003",
  setEnv: "http://127.0.0.1:30004",
  envEnv: "http://127.0.0.1:30005",
  rqDashboard: "http://0.0.0.0:9181",
  username: "honeypy_web_app",
  password: "P@r41LaX?!",
  defaultQueue: "normal-queue"
};
