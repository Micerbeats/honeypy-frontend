export const environment = {
  production: true,
  auth: false,
  // testEnv: "http://www.honeypy.net/api/test",
  // reportEnv: "http://www.honeypy.net/api/report",
  // hostEnv: "http://www.honeypy.net/api/host",
  // setEnv: "http://www.honeypy.net/api/set",
  // rqDashboard: "http://rq-dashboard:9181/",
  // redirectUri: "http://www.honeypy.net/",
  testEnv: "http://test-service.default.svc.cluster.local",
  envEnv: "http://environment-service.default.svc.cluster.local",
  reportEnv: "http://report-service.default.svc.cluster.local",
  hostEnv: "http://host-service.default.svc.cluster.local",
  setEnv: "http://set-service.default.svc.cluster.local",
  rqDashboard: "http://rq-dashboard.default.svc.cluster.local",
  url: "http://frontend-service.default.svc.cluster.local",
  username: "honeypy_web_app",
  password: "P@r41LaX?!",
  defaultQueue: "normal-queue"
};
