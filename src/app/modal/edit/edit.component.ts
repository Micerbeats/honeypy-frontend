import { Component } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { PanelService } from '../../shared/services/panel.services';
import { SetService } from '../../shared/services/set.services';
import { TestService } from '../../shared/services/test.services';
import { HostService } from '../../shared/services/host.services';
import { HttpService } from '../../shared/services/http.services';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent {

  type: string;
  panels: any;

  name: string;
  set: any;
  browsers: any;
  hosts: any = [];

  constructor(private panelService: PanelService, private httpService: HttpService, private testService: TestService, private setService: SetService, private hostService: HostService, private router: Router) {

    this.browsers = this.testService.browsers;

    if (!this.panels) {
      this.panels = this.panelService.panels;
      this.type = this.panels.modal.kind;
    }

    this.panelService.panelsEvent.subscribe(
      (panels) => {
        this.panels = panels;
        this.type = this.panels.modal.kind;
      }
    );

    if (!this.set) {
      this.set = this.setService.set;
      this.saveSetAttributes();
    }

    this.setService.setEvent.subscribe(
      (set) => {
        this.set = set;
        this.saveSetAttributes();
      }
    );

    this.hostService.hostsEvent.subscribe(
      (hosts) => {
        this.hosts = hosts;
      }
    );

    if (!this.hostService.hosts) {
      this.hosts = this.hostService.getHosts();
    }
    else {
      this.hosts = this.hostService.hosts;
    }

  }

  saveSetAttributes() {
    this.name = this.set.name;
  }

  submit() {
    if (this.set) {
      this.saveSet();
    }
  }

  saveSet() {
    this.httpService.saveSet(this.name, this.set).subscribe(
      (data) => {
        if (this.name != this.set.name) {
          this.setService.fetchSets();
          this.router.navigate(["set", this.set.name]);
        }
        this.setService.setSet(this.set);
        this.panelService.showAlert("Set Saved");
        this.panelService.closeModal();
      },
      (error) => {
        this.panelService.showAlert("Error saving set");
        console.error(error.error);
      }
    );
  }

  cancel() {
    this.panelService.closeModal();
  }

  close() {
    this.cancel();
  }
}
