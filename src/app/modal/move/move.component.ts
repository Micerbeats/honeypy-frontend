import { Component } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { PanelService } from '../../shared/services/panel.services';
import { TestService } from '../../shared/services/test.services';
import { HttpService } from '../../shared/services/http.services';

@Component({
  selector: 'app-move',
  templateUrl: './move.component.html',
  styleUrls: ['./move.component.scss']
})
export class MoveComponent {

  type: string;
  panels: any;

  // source
  sourcePathError: boolean = false;
  sourcePathMessage: string = "";

  // destination
  destinationPathError: boolean = false;
  destinationPathMessage: string = "";

  // phrase
  phraseSourceError: boolean = false;
  phraseSourceMessage: string = "";
  phraseDestinationError: boolean = false;
  phraseDestinationMessage: string = "";

  // set
  setName: string = "";
  setNameError: boolean = false;

  // feature
  feature = {
    kind: 'feature',
    source: '',
    destination: ''
  };

  // Phrase
  phrase = {
    kind: 'phrase',
    phrase: "",
    source: "",
    destination: ""
  };

  constructor(private panelService: PanelService, private httpService: HttpService, private testService: TestService, private router: Router) {
    if (!this.panels) {
      this.panels = this.panelService.panels;
      if (this.type != this.panels.modal.kind) {
        this.setForm();
      }
      this.type = this.panels.modal.kind;
    }

    this.panelService.panelsEvent.subscribe(
      (panels) => {
        this.panels = panels;
        if (this.type != this.panels.modal.kind) {
          this.setForm();
        }
        this.type = this.panels.modal.kind;
      }
    );

    this.panelService.modalItemEvent.subscribe(
      (data) => {
        this.generate(data);
      }
    );

    this.testService.fileEvent.subscribe(
      (file) => {
        this.generate(file)
      }
    );
    this.setForm();
  }

  cancel() {
    this.panelService.closeModal();
    this.resetForms();
  }

  close() {
    this.cancel();
  }

  setForm() {
    if (this.panelService.modalItem !== undefined) {
      this.resetForms();
      this.generate(this.panelService.modalItem);
    }
    else {
      this.resetForms();
      this.generate(this.testService.file);
    }
  }

  generate(data) {
    if (data) {
      if (this.type == "feature") {
        this.feature.source = data.path;
      }
      else if (this.type == "phrase") {
        this.phrase.source = data.path;
        this.phrase.phrase = data.phrase;
      }
    }
  }

  move() {
    if (this.type == "feature") {
      if (this.panels.modal.type == "copy") {
        this.copyFeature();
      }
      else if (this.panels.modal.type == "move") {
        this.moveFeature();
      }
    }
    else if (this.type == "phrase") {
      if (this.panels.modal.type == "copy") {
        this.copyPhrase();
      }
      else if (this.panels.modal.type == "move") {
        this.movePhrase();
      }
    }
  }

  copyFeature() {
    this.httpService.copyFile(this.feature).subscribe(
      (data) => {
        this.testService.getFeatures();
        this.panelService.closeModal();
        let params = {};
        params['feature'] = this.feature.destination;
        let navigationExtras: NavigationExtras = {
          queryParams: params
        };
        this.router.navigate(['editor'], navigationExtras);
        this.panelService.setModalItem(undefined);
      },
      (error) => {
        this.panelService.showAlert("Error copying feature");
        this.featureErrors(error.error);
      }
    );
  }

  copyPhrase() {
    this.httpService.copyFile(this.phrase).subscribe(
      (data) => {
        this.testService.getPhrases();
        this.panelService.closeModal();
        let params = {};
        params['phrase'] = this.phrase.destination;
        let navigationExtras: NavigationExtras = {
          queryParams: params
        };
        this.router.navigate(['editor'], navigationExtras);
        this.panelService.setModalItem(undefined);
      },
      (error) => {
        this.panelService.showAlert("Error moving phrase");
        this.phraseErrors(error.error);
      }
    );
  }


  //
  // Move/Rename
  //

  moveFeature() {
    this.httpService.moveFile(this.feature).subscribe(
      (data) => {
        this.testService.getFeatures();
        this.panelService.closeModal();
        let params = {};
        params['feature'] = this.feature.destination;
        let navigationExtras: NavigationExtras = {
          queryParams: params
        };
        this.router.navigate(['editor'], navigationExtras);
        this.panelService.setModalItem(undefined);
      },
      (error) => {
        this.panelService.showAlert("Error moving feature");
        this.featureErrors(error.error);
      }
    );
  }

  movePhrase() {
    this.httpService.moveFile(this.phrase).subscribe(
      (data) => {
        this.testService.getPhrases();
        this.panelService.closeModal();
        let params = {};
        params['phrase'] = this.phrase.destination;
        let navigationExtras: NavigationExtras = {
          queryParams: params
        };
        this.router.navigate(['editor'], navigationExtras);
        this.panelService.setModalItem(undefined);
      },
      (error) => {
        this.panelService.showAlert("Error moving phrase");
        this.phraseErrors(error.error);
      }
    );
  }

  featureErrors(errors) {
    for (let key in errors) {
      if (key == "source") {
        this.sourcePathError = true;
        this.sourcePathMessage = errors[key][0];
      }
      else if (key == "destination") {
        this.destinationPathError = true;
        this.destinationPathMessage = errors[key][0];
      }
    }
  }

  phraseErrors(errors) {
    for (let key in errors) {
      if (key == "source") {
        this.sourcePathError = true;
        this.sourcePathMessage = errors[key][0];
      }
      else if (key == "destination") {
        this.destinationPathError = true;
        this.destinationPathMessage = errors[key][0];
      }
      else if (key == "phrase") {
        this.phraseDestinationError = true;
        this.phraseDestinationMessage = errors[key][0];
      }
    }
  }

  //
  // form validation
  //

  sourcePathChange() {
    this.sourcePathError = false;
    this.sourcePathMessage = "";
  }

  destinationPathChange() {
    this.destinationPathError = false;
    this.destinationPathMessage = "";
  }

  phraseSourceChange() {
    this.phraseSourceError = false;
    this.phraseSourceMessage = "";
  }

  phraseDestinationChange() {
    this.phraseDestinationError = false;
    this.phraseDestinationMessage = "";
  }

  resetForms() {
    this.feature.source = "";
    this.feature.destination = "";

    this.phrase.phrase = "";
    this.phrase.destination = "";
    this.phrase.source = "";

    this.sourcePathChange();
    this.destinationPathChange();
    this.phraseSourceChange();
    this.phraseDestinationChange();
  }
}
