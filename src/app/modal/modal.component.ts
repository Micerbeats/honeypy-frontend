import { Component, Input } from '@angular/core';
import { PanelService } from "../shared/services/panel.services";
import { HttpService } from "../shared/services/http.services";
import { TestService } from "../shared/services/test.services";
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ["./modal.component.scss"]
})

export class ModalComponent {

  modalItem: any;
  modalType: string;

  panels: any;
  params: any = {};
  visibility: any = false;
  menu: any = [];
  input: string;
  message: string;

  selectedOption;
  options = [
    "feature",
    "phrase",
    "set"
  ];

  constructor(private panelService: PanelService, private httpService: HttpService, private testService: TestService, private router: Router, private activatedRoute: ActivatedRoute) {
    if (!this.panels) {
      this.panels = this.panelService.panels;
      this.getType();
    }

    this.panelService.panelsEvent.subscribe(
      (panels) => {
        this.panels = panels;
        this.getType();
      }
    );

    this.panelService.modalItemEvent.subscribe(
      (data) => {
        this.modalItem = data;
        if (this.modalItem) {
          this.selectedOption = this.modalItem.kind;
        }
        else {
          this.selectedOption = this.options[0];
        }
        this.onSelect(this.selectedOption);
      }
    );

    this.menu = [
      this.panels.modal.menu.feature,
      this.panels.modal.menu.phrase,
      this.panels.modal.menu.set
    ];
  }

  onSelect(option) {
    this.panelService.setModalFileType(option);
  }

  getType() {
    this.selectedOption = this.panels.modal.kind;
    this.modalType = this.panels.modal.type;
  }

  onClick(event) {
    if (event.target.className != "modal__form-dropdown-label") {
      this.visibility = false;
    }
  }

  selectType(option) {
    this.selectedOption = option;
    this.visibility = false;
  }

  onInputChange() {
    if (this.message) {
      this.message = "";
    }
  }

  close() {
    this.visibility = false;
    this.message = "";
    this.input = "";
    this.panelService.closeModal();
    this.panelService.setModalItem(undefined);
  }

}
