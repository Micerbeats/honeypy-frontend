import { Component } from '@angular/core';
import { PanelService } from '../../shared/services/panel.services';
import { TestService } from '../../shared/services/test.services';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent {

  file: any;

  constructor(private panelService: PanelService, private testService: TestService) {
    this.file = this.testService.file;

    this.testService.fileEvent.subscribe(
      (file) => {
        this.file = file;
      }
    );
  }

  cancel() {
    this.panelService.closeModal();
  }

  close() {
    this.cancel();
  }
}
