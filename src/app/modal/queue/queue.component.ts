import { Component } from '@angular/core';
import { QueueService } from '../../shared/services/queue.services';
import { PanelService } from '../../shared/services/panel.services';

@Component({
  selector: 'app-queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.scss']
})
export class QueueComponent {

  queue: string;
  original: string;

  constructor(private queueService: QueueService, private panelService: PanelService) {

    this.queueService.queueEvent.subscribe(
      (queue) => {
        this.queue = queue;
        this.setOriginal();
      }
    );

    if (!this.queue) {
      this.queue = this.queueService.queue;
      this.setOriginal();
    }

  }

  setOriginal() {
    this.original = this.queue;
  }

  cancel() {
    this.queue = this.original;
    this.panelService.closeModal();
  }

  save() {
    if (this.queue.length > 0) {
      this.queueService.setQueue(this.queue);
      this.cancel();
    }
  }
}
