import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { PanelService } from '../../shared/services/panel.services';
import { TestService } from '../../shared/services/test.services';
import { HttpService } from '../../shared/services/http.services';
import { SetService } from '../../shared/services/set.services';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent {

  input: string;
  type: string;
  kind: string;
  modalItem: any;
  inputError: boolean = false
  inputErrorMessage: string = "";

  constructor(
    private panelService: PanelService,
    private httpService: HttpService,
    private testService: TestService,
    private setService: SetService,
    private router: Router,
    private activatedRoute: ActivatedRoute) {
    this.panelService.panelsEvent.subscribe(
      (panels) => {
        if (this.kind != panels.modal.kind) {
          this.kind = panels.modal.kind;
          this.resetForms();
        }
      }
    );

    this.panelService.modalItemEvent.subscribe(
      (item) => {
        this.setInput(item);
      }
    );

    this.testService.fileEvent.subscribe(
      (file) => {
        this.setInput(file);
      }
    );

    if (!this.modalItem) {
      this.modalItem = this.panelService.modalItem;
      this.setInput(this.modalItem);
    }

    if (!this.kind) {
      if (this.testService.file) {
        if (this.testService.file.kind) {
          this.kind = this.testService.file.kind;
        }
      }
      else if (this.setService.set) {
        this.kind = "set";
      }
      else {
        this.kind = "feature";
      }
    }
  }

  setInput(data) {
    if (data) {
      if ( (data.kind == "feature") || (data.kind == "phrase")) {
        this.input = data.path;
      }
      else if (data.kind == "set") {
        this.input = data.name
      }
    }
  }

  cancel() {
    this.panelService.closeModal();
    this.resetForms();
  }

  close() {
    this.cancel();
  }

  resetForms() {
    this.inputError = false;
    this.inputErrorMessage = "";
  }

  delete() {
    if (this.kind == "feature") {
      this.deleteFeature();
    }
    else if (this.kind == "phrase") {
      this.deletePhrase();
    }
    else if (this.kind == "set") {
      this.deleteSet();
    }
  }

  deleteFeature() {
    this.httpService.deleteFile("feature", this.input).subscribe(
      (data) => {
        this.route(this.input);
        this.testService.getFeatures();
        this.panelService.closeModal();
        this.panelService.showAlert("Feature File Deleted");
        this.panelService.setModalItem(undefined);
      },
      (error) => {
        this.panelService.showAlert("Error deleting feature");
        this.inputErrors(error.error);
      }
    );
  }

  deletePhrase() {
    this.httpService.deleteFile("phrase", this.input).subscribe(
      (data) => {
        this.route(this.input);
        this.testService.getPhrases();
        this.panelService.closeModal();
        this.panelService.showAlert("Phrase File Deleted");
        this.panelService.setModalItem(undefined);
      },
      (error) => {
        this.panelService.showAlert("Error deleting phrase");
        this.inputErrors(error.error);
      }
    );
  }

  deleteSet() {
    this.httpService.deleteSet(this.input).subscribe(
      (data) => {
        this.panelService.closeModal();
        this.setService.setSet(undefined);
        this.panelService.setModalItem(undefined);
        this.setService.fetchSets();
        this.panelService.showAlert("Set Deleted");
        this.route(this.input);
      },
      (error) => {
        this.panelService.showAlert("Error deleting set");
        this.inputErrors(error.error);
      }
    );
  }

  inputErrors(errors) {
    for (let key in errors) {
      if (key == "path") {
        this.inputError = true;
        this.inputErrorMessage = errors[key][0];
      }
    }
  }

  onInputChange() {
    if (this.inputError === true) {
      this.resetForms();
    }
  }

  route(path) {
    // Check whether app should direct user to another page
    if (this.activatedRoute["queryParams"]["_value"][this.kind] == path) {
      this.router.navigate(["/"]);
    }
  }
}
