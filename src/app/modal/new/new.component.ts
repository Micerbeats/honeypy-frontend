import { Component } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { PanelService } from '../../shared/services/panel.services';
import { TestService } from '../../shared/services/test.services';
import { SetService } from '../../shared/services/set.services';
import { HttpService } from '../../shared/services/http.services';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent {

  panels: any;
  type: string;

  // Phrase
  phrasePathError: boolean = false;
  phrasePathErrorMessage: string = "";
  phraseError: boolean = false;
  phraseErrorMessage: string = "";
  phrase = {
    kind: 'phrase',
    phrase: "",
    path: ""
  };

  // feature
  featurePathError: boolean = false;
  featurePathErrorMessage:string  = ""
  feature = {
    kind: 'feature',
    path: ""
  };

  // set
  setNameError: boolean = false;
  setNameErrorMessage: string = "";
  set = {
    name: "",
    features: []
  };

  constructor(private panelService: PanelService, private testService: TestService, private setService: SetService, private httpService: HttpService, private router: Router) {
    this.panelService.panelsEvent.subscribe(
      (panels) => {
        this.panels = panels;
        this.setForm();
      }
    );

    this.panelService.modalItemEvent.subscribe(
      (data) => {
        this.generate(data);
      }
    );

    this.testService.fileEvent.subscribe(
      (file) => {
        this.generate(file)
      }
    );
    if (!this.panels) {
      this.panels = this.panelService.panels;
    }

    if (!this.type) {
      this.setForm();
    }
  }

  generate(data) {
    if (data) {
      if (this.type == "feature") {
        this.feature.path = data.path;
      }
      else if (this.type == "phrase") {
        this.phrase.path = data.path;
        this.phrase.phrase = data.phrase;
      }
      else if (this.type == "set") {
        this.set.name = data.name;
      }
    }
  }

  setForm() {
    this.type = this.panels.modal.kind;
    if (this.panelService.modalItem !== undefined) {
      this.resetForms();
      this.generate(this.panelService.modalItem);
    }
    else {
      this.resetForms();
      this.generate(this.testService.file);
    }
  }


  cancel() {
    this.panelService.closeModal();
    this.resetForms();
  }

  close() {
    this.cancel();
  }

  //
  // Create
  //

  create() {
    if (this.panels.modal.kind == "feature") {
      this.createFeature();
    }
    else if (this.panels.modal.kind == "phrase") {
      this.createPhrase();
    }
    else if (this.panels.modal.kind == "set") {
      this.createSet();
    }
  }

  createFeature() {
    this.httpService.createFile(this.feature).subscribe(
      (data) => {
        this.testService.getFeatures();
        this.panelService.closeModal();
        if (this.testService.ifFile(this.feature.path)) {
          let params = {};
          params['feature'] = this.feature.path;
          let navigationExtras: NavigationExtras = {
            queryParams: params
          };
          this.router.navigate(['editor'], navigationExtras);
        }
      },
      (error) => {
        this.panelService.showAlert("Error creating feature");
        this.featureErrors(error.error);
      }
    );
  }

  createPhrase() {
    this.httpService.createFile(this.phrase).subscribe(
      (data) => {
        this.testService.getPhrases();
        this.panelService.closeModal();
        if (this.testService.ifFile(this.phrase.path)) {
          let params = {};
          params['phrase'] = this.phrase.path;
          let navigationExtras: NavigationExtras = {
            queryParams: params
          };
          this.router.navigate(['editor'], navigationExtras);
        }
      },
      (error) => {
        this.panelService.showAlert("Error creating phrase");
        this.phraseErrors(error.error);
      }
    );
  }

  createSet() {
    if (this.set.name.length > 0) {
      this.httpService.createSet(this.set).subscribe(
        (data) => {
          this.setService.fetchSets();
          this.router.navigate(['set', this.set.name]);
          this.panelService.closeModal();
        },
        (error) => {
          this.panelService.showAlert("Error creating the set");
          this.setErrors(error.error);
        }
      );
    }
  }

  //
  // Dom functions
  //

  featurePathInput() {
    if (this.featurePathError === true) {
      this.featurePathError = false;
    }
  }

  phraseInput() {
    if (this.phraseError === true) {
      this.phraseError = false;
    }
  }

  phrasePathInput() {
    if (this.phrasePathError === true) {
      this.phrasePathError = false;
    }
  }

  setNameInput() {
    if (this.setNameError === true) {
      this.setNameError = false;
    }
  }

  setName() {
    if (this.setNameError === true) {
      this.setNameError = false;
    }
  }

  //
  // error handlers
  //

  featureErrors(errors) {
    for (let key in errors) {
      if (key == "path") {
        this.featurePathError = true;
        this.featurePathErrorMessage = errors[key][0];
      }
    }
  }

  phraseErrors(errors) {
    for (let key in errors) {
      if (key == "path") {
        this.phrasePathError = true;
        this.phrasePathErrorMessage = errors[key][0]
      }
      else if (key == "phrase") {
        this.phraseError = true;
        this.phraseErrorMessage = errors[key][0]
      }
    }
  }

  setErrors(errors) {
    for (let key in errors) {
      if (key == "name") {
        this.setNameError = true;
        this.setNameErrorMessage = errors[key][0];
      }
    }
  }

  //
  // reset form
  //

  resetForms() {
    // features
    this.featurePathError = false;
    this.featurePathErrorMessage = "";
    this.feature = {
        kind: 'feature',
        path: ""
      };
    // phrases
    this.phrasePathError = false;
    this.phrasePathErrorMessage = "";
    this.phraseError = false;
    this.phraseErrorMessage = "";
    this.phrase = {
      kind: 'phrase',
      phrase: "",
      path: ""
    };
    // sets
    this.setNameError = false;
    this.setNameErrorMessage = "";
    this.set = {
        name: "",
        features: []
      };
  }
}
