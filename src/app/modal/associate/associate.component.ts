import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../../shared/services/http.services';
import { PanelService } from '../../shared/services/panel.services';
import { TestService } from '../../shared/services/test.services';
import { SetService } from '../../shared/services/set.services';

@Component({
  selector: 'app-associate',
  templateUrl: './associate.component.html',
  styleUrls: ['./associate.component.scss']
})
export class AssociateComponent {

  featurePath: string;
  setName: string;
  modalItem: any;

  setNameError: boolean = false
  setNameErrorMessage: string = "";

  featuresPathError: boolean = false
  featuresPathErrorMessage: string = "";

  constructor(
    private activatedRoute: ActivatedRoute,
    private panelService: PanelService,
    private httpService: HttpService,
    private setService: SetService,
    private testService: TestService,
  ) {
    this.panelService.modalItemEvent.subscribe(
      (item) => {
        this.setModalItem(item);
      }
    );

    if (!this.modalItem) {
      this.setModalItem(this.panelService.modalItem);
    }

    this.setService.setEvent.subscribe(
      (set) => {
        this.setSetName(set);
      }
    );

    this.panelService.panelsEvent.subscribe(
      (panels) => {
        this.setSetName(this.setService.set);
      }
    );
    this.setSetName(this.setService.set);
  }

  setSetName(set) {
    if (set) {
      this.setName = set.name;
    }
  }

  setModalItem(item) {
    if (item) {
      this.modalItem = item;
      this.featurePath = this.modalItem.path;
    }
    else {
      this.reset();
    }
  }

  cancel() {
    this.panelService.closeModal();
    this.panelService.setModalItem(undefined);
  }

  submit() {
    this.associateFeature();
  }

  reset() {
    this.featurePath = "";
    this.setName = "";

    this.resetFeaturePathInput();
    this.resetSetNameInput();
  }

  associateFeature() {
    if (this.featurePath) {
      this.httpService.associateSet(this.setName, [this.featurePath]).subscribe(
        (data) => {
          this.panelService.showAlert("Associated feature to set");
          if (this.setService.set) {
            if (this.setService.set.name == this.setName) {
                this.setService.fetchSet(this.setName);
            }
          }
          else if (this.activatedRoute["queryParams"]["_value"].hasOwnProperty("feature")) {
            if (this.activatedRoute["queryParams"]["_value"]["feature"] == this.featurePath) {
                this.testService.fetchFile("feature", this.featurePath);
            }
          }
          this.reset();
          this.cancel();
        },
        (error) => {
          this.handleErrors(error.error);
          console.error(error.error);
        }
      );
    }
  }

  handleErrors(errors) {
    for (let key in errors) {
      if (key == "features") {
        this.featuresPathError = true;
        this.featuresPathErrorMessage = errors[key][0][0][0];
      }
      if (key == "name") {
        this.setNameError = true;
        this.setNameErrorMessage = errors[key][0];
      }
    }
  }

  resetSetNameInput() {
    this.setNameError = false;
    this.setNameErrorMessage = "";
  }

  resetFeaturePathInput() {
    this.featuresPathError = false;
    this.featuresPathErrorMessage = "";
  }

}
