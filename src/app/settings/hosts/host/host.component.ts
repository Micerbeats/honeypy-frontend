import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { HostService } from "../../../shared/services/host.services";
import { HttpService } from "../../../shared/services/http.services";
import { PanelService } from "../../../shared/services/panel.services";
import { SettingsService } from "../../../shared/services/settings.services";

@Component({
  selector: 'app-host',
  templateUrl: './host.component.html',
  styleUrls: ['./host.component.scss']
})
export class HostComponent {

  name: string;
  ifHost: boolean = false;
  form: boolean = false;
  host: any = {};
  submitButton: string;
  errors = [];
  formError = {
    'name': {
      'error':false,
      'message':""
    },
    'location': {
      'error':false,
      'message':""
    }
  };

  constructor(private hostService: HostService, private settingsService: SettingsService, private httpService: HttpService, private panelService: PanelService, private location: Location, private router: Router) {
    this.host = this.hostService.host;
    this.getHostName();
  }

  getHostName() {
    if (this.host) {
      if (!this.host.hasOwnProperty("name")) {
          this.ifHost = false;
          this.generateHost();
      }
      else {
        this.ifHost = true;
        const name = this.host.name;
        this.name = name;
        this.submitButton = "Save";
      }
    }
    else {
      this.ifHost = false;
      this.generateHost();
    }
    this.settingsService.closeAdd();
  }

  generateHost() {
    this.submitButton = "Create";
    this.host = {};
    this.host.name = "";
    this.host.location = "";
    this.host.description = "";
    this.host.active = false;
  }

  toggleForm() {
    this.form = !this.form;
  }

  exitForm() {
    this.router.navigate(["settings", "hosts"]);
  }

  submit() {
    if (this.submitButton == "Save") {
      this.httpService.saveHost(this.name, this.host).subscribe(
        (data) => {
          this.panelService.showAlert("Host Saved");
          this.hostService.getHosts();
          this.hostService.setHost(this.host.name);
          this.router.navigate(["settings", "hosts"]);
          this.toggleForm();
        },
        (error) => {
          this.errors = error.error.errors;
          this.hostService.setHost(undefined);
          this.setErrors();
        }
      );
    }
    else if (this.submitButton == "Create") {
      this.httpService.createHost(this.host).subscribe(
        (data) => {
          this.panelService.showAlert("Host Created");
          this.hostService.getHosts();
          this.router.navigate(["settings", "hosts"]);
        },
        (error) => {
          this.errors = error.error.errors;
          this.setErrors();
        }
      );
    }
  }

  setErrors() {
    for (let error of this.errors) {
      if (error.field == "name") {
        this.formError.name.message = error.error;
        this.formError.name.error = true;
      }
      else if (error.field == "location") {
        this.formError.location.message = error.error;
        this.formError.location.error = true;
      }
    }
  }

  onNameChange(event) {
    if (this.formError.name.error === true) {
      this.formError.name.error = false;
    }
  }

  onLocationChange(event) {
    if (this.formError.location.error === true) {
      this.formError.location.error = false;
    }
  }

  deleteHost() {
    this.httpService.deleteHost(this.host.name).subscribe(
      (data) => {
        this.panelService.showAlert("Host Deleted");
        this.hostService.getHosts();
        this.router.navigate(["settings", "hosts"]);
      },
      (error) => {
        console.log(error);
        this.panelService.showAlert("Error Deleting Host");
      }
    );
  }
}
