import { Component } from '@angular/core';
import { HostService } from '../../shared/services/host.services';
import { SettingsService } from '../../shared/services/settings.services';
import { HttpService } from '../../shared/services/http.services';
import { PanelService } from '../../shared/services/panel.services';

@Component({
  selector: 'app-hosts',
  templateUrl: './hosts.component.html',
  styleUrls: ['./hosts.component.scss']
})
export class HostsComponent {

  title: string;
  hosts = [];
  host: any = {};
  ifForm: boolean = false;
  createTooltip: string;

  errors = [];

  constructor(private hostService: HostService, private httpService: HttpService, private panelService: PanelService, private settingsService: SettingsService) {
    this.hostService.hostsEvent.subscribe(
      (hosts) => {
        this.hosts = hosts;
      }
    );

    this.hostService.getHosts();

    this.hostService.hostEvent.subscribe(
      (host) => {
        this.host = host;
      }
    );

    if (!this.createTooltip) {
      this.setCreateToolTip();
    }
    this.settingsService.setTitle("Host Management");
    this.settingsService.setHeader("hosts");
    this.settingsService.closeAdd();
  }

  toggleCreate() {
    this.ifForm = !this.ifForm;
    this.setCreateToolTip();
  }

  setCreateToolTip() {
    if (this.ifForm === true) {
      this.createTooltip = "Cancel";
    }
    else {
      this.createTooltip = "Create";
    }
  }
}
