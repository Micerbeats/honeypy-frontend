import { Component, EventEmitter } from '@angular/core';
import { EnvironmentService } from '../../../shared/services/environment.services';
import { SettingsService } from '../../../shared/services/settings.services';
import { HttpService } from '../../../shared/services/http.services';
import { PanelService } from '../../../shared/services/panel.services';

@Component({
  selector: 'app-environment',
  templateUrl: "./environment.component.html",
  styleUrls: ["./environment.component.scss"]
})

export class EnvironmentComponent {

  objectKeys = Object.keys;
  environment: any;
  newVariable = {
    name: {
      error: false,
      value: ""
    },
    value: {
      error: false,
      value: ""
    }
  }
  ifAdd: boolean = false;

  constructor(
    private envService: EnvironmentService,
    private settingsService: SettingsService,
    private httpService: HttpService,
    private panelService: PanelService
  ) {
    this.settingsService.ifAddEvent.subscribe(
      (bool) => {
        this.ifAdd = bool;
      }
    );

    this.envService.environmentEvent.subscribe(
      (environment) => {
        this.sortVariables(environment)
        this.setTitle();
      }
    );

    if (!this.environment) {
      let environment = this.envService.environment;
      this.sortVariables(environment);
    }

    this.setTitle();
    this.settingsService.setHeader("environment");
    this.settingsService.closeAdd();
  }

  sortVariables(environment) {
    let newVariables = {};
      Object.keys(environment.variables).sort().forEach(function(key) {
        newVariables[key] = environment.variables[key];
      });
      environment.variables = newVariables;
      this.environment = environment;
  }

  setTitle() {
    if (this.environment) {
      this.settingsService.setTitle(this.environment.name);
    }
  }

  add() {
    this.newVariable.name.value = this.newVariable.name.value.trim();
    this.httpService.addVariable(this.environment.name, this.newVariable.name.value, this.newVariable.value.value).subscribe(
      (data) => {
        this.envService.getEnvironment(this.environment.name);
        this.panelService.showAlert("Added Variable");
        this.resetNewVariabe();
        this.settingsService.closeAdd();
      },
      (errors) => {
        this.panelService.showAlert("Error Adding the Variable");
        console.error(errors.error);
      }
    );
  }

  remove(variable) {
    if (this.ifAdd) {
      let result = confirm("Are you sure you want to remove the variable?");
      if (result) {
        this.httpService.removeVariable(this.environment.name, variable).subscribe(
          (data) => {
            this.envService.getEnvironment(this.environment.name);
            this.panelService.showAlert("Removed Variable");
            this.resetNewVariabe();
          },
          (errors) => {
            this.panelService.showAlert("Error Removing the Variable");
            console.error(errors.error);
          }
        );
      }
    }
  }

  resetNewVariabe() {
    this.newVariable.name.error = false;
    this.newVariable.name.value = "";
    this.newVariable.value.error = false;
    this.newVariable.value.value = "";
  }

}
