import { Component, EventEmitter, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { EnvironmentService } from '../../shared/services/environment.services';
import { SettingsService } from '../../shared/services/settings.services';
import { HttpService } from '../../shared/services/http.services';
import { PanelService } from '../../shared/services/panel.services';

@Component({
  selector: 'app-environments',
  templateUrl: "./environments.component.html",
  styleUrls: ["./environments.component.scss"]
})

export class EnvironmentsComponent implements OnDestroy {

  environments: any;
  selectedEnvironment: string;
  newEnvName: string = "";
  ifAdd: boolean = false;

  constructor(private envService: EnvironmentService,
    private settingsService: SettingsService,
    private httpService: HttpService,
    private panelService: PanelService,
    private ref: ChangeDetectorRef
  ) {
    this.settingsService.ifAddEvent.subscribe(
      (bool) => {
        this.ifAdd = bool;
      }
    );

    this.envService.environmentsEvent.subscribe(
      (environments) => {
        this.environments = environments;
      }
    );

    this.envService.selectedEnvironmentEvent.subscribe(
      (env) => {
        this.selectedEnvironment = env;
        if (!this.ref['destroyed']) {
            this.ref.detectChanges();
        }
      }
    );

    if (!this.selectedEnvironment) {
      this.selectedEnvironment = this.envService.selectedEnvironment;
    }

    if (!this.envService.environments) {
      this.envService.fetchEnvironments();
    }
    else {
      this.environments = this.envService.environments;
    }

    this.settingsService.setTitle("Environments");
    this.settingsService.setHeader("environments");
  }

  create() {
    if (this.newEnvName) {
      this.newEnvName = this.newEnvName.trim();
      let env = {
        name:this.newEnvName
      };
      this.httpService.createEnvironment(env).subscribe(
        (data) => {
          this.envService.fetchEnvironments();
          this.panelService.showAlert("Environment Created: " + this.newEnvName);
          this.resetNewEnvForm();
        },
        (errors) => {
          this.panelService.showAlert("Error Creating a New Environment");
          console.error(errors.error);
        }
      );
    }
  }

  resetNewEnvForm() {
    this.settingsService.closeAdd();
    this.newEnvName = "";
  }

  selectEnvironment(env) {
    env.hover = false;
    this.envService.selectEnvironment(env.name);
  }

  resetSelectedEnvironment() {
    this.selectEnvironment({name:""});
  }

  ngOnDestroy() {
    this.ref.detach();
    // this.settingsService.ifAddEvent.unsubscribe();
    // this.envService.environmentsEvent.unsubscribe();
    // this.envService.selectedEnvironmentEvent.unsubscribe();
  }
}
