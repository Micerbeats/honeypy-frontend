import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SettingsService } from '../shared/services/settings.services';
import { HostService } from '../shared/services/host.services';
import { HttpService } from '../shared/services/http.services';
import { PanelService } from '../shared/services/panel.services';
import { EnvironmentService } from '../shared/services/environment.services';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent {

  title: string = "Settings";
  header: string;
  environment: any;
  selectedEnvironment: any;
  ifAdd: boolean = false;

  constructor(
    private settingsService: SettingsService,
    private hostService: HostService,
    private httpService: HttpService,
    private panelService: PanelService,
    private router: Router,
    private envService: EnvironmentService
  ) {

    this.title = this.settingsService.title;

    this.settingsService.ifAddEvent.subscribe(
      (bool) => {
        this.ifAdd = bool;
      }
    );

    this.settingsService.titleEvent.subscribe(
      (title) => {
        this.title = title;
      }
    );

    this.settingsService.headerEvent.subscribe(
      (header) => {
        this.header = header;
      }
    );

    this.envService.environmentEvent.subscribe(
      (environment) => {
        this.environment = environment;
      }
    );

    if (!this.environment) {
      this.environment = this.envService.environment;
    }

    this.header = this.settingsService.header;

    this.envService.selectedEnvironmentEvent.subscribe(
      (env) => {
        this.selectedEnvironment = env;
      }
    );

    if (!this.selectedEnvironment) {
      this.selectedEnvironment = this.envService.selectedEnvironment;
    }
  }

  back() {
    this.settingsService.closeAdd();
    this.router.navigate(["settings", "environments"]);
  }

  setTitle(title) {
    this.settingsService.setTitle(title);
  }

  setHeader(header) {
    this.settingsService.setHeader(header);
  }

  closeAdd() {
    this.settingsService.closeAdd();
  }

  create(name) {
    if (this.header == "hosts") {
      this.createHost();
    }
    else {
      this.createEnvironment(name);
    }
  }

  createHost() {
    this.hostService.setHost(undefined);
    this.router.navigate(["settings", "hosts", "new"]);
  }

  createEnvironment(name) {
    this.settingsService.openAdd();
  }

  selectEnvironment() {
    if (this.environment.name == this.selectedEnvironment) {
      this.envService.selectEnvironment(undefined);
    }
    else {
      this.envService.selectEnvironment(this.environment.name);
    }
  }

  navigateRqDasboard() {
    var win = window.open(environment.rqDashboard, '_blank');
    win.focus();
  }

  delete() {
    let result = confirm("Are you sure you want to delete the environment?");
    if ( (this.envService.environment) && (result) ) {
      this.httpService.deleteEnvironment(this.envService.environment.name).subscribe(
        (data) => {
          this.panelService.showAlert("Environment Deleted");
          this.settingsService.closeAdd();
          if (this.settingsService.ifAdd) {
              this.settingsService.toggleAdd();
          }
          this.envService.fetchEnvironments();
          this.router.navigate(["settings", "environments"]);
        },
        (errors) => {
          this.panelService.showAlert("Error Deleting Environment");
          console.error(errors.error);
        }
      );
    }
  }
}
