import { Component, OnInit } from '@angular/core';
import { PanelService } from '../shared/services/panel.services';
import { ReportService } from '../shared/services/report.services';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent {

  panels: any;
  report: any;
  tabs = [
    {
      'label': 'Report',
      'active': true
    },
    {
      'label': 'Properties',
      'active': false
    }
  ];
  selectedTab = this.tabs[0];

  constructor(private panelService: PanelService, private reportService: ReportService) {
    if (!this.panels) {
      this.panels = this.panelService.panels;
    }

    this.panelService.panelsEvent.subscribe(
      (panels) => {
        this.panels = panels;
      }
    );

    this.reportService.reportEvent.subscribe(
      (report) => {
        this.report = report;
        this.tabs[0].label = this.report.name;
        this.fetchReport();
      }
    );

  }

  fetchReport() {
    if (this.reportService.running === true) {
      setTimeout( () => {
        if (!this.report.hasOwnProperty("end")) {
          this.reportService.fetchReport();
        }
        else {
          this.reportService.toggleRunStatus();
        }
      }, 1500);
    }
  }

  selectTab(tab) {
    this.selectedTab = tab;
    for (var i = 0; i < this.tabs.length; i++) {
      if (this.tabs[i].label == this.selectedTab.label) {
        this.tabs[i].active = true;
      }
      else {
        this.tabs[i].active = false;
      }
    }
  }

}
