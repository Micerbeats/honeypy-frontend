import { Component, Input, OnChanges } from '@angular/core';
import { ReportService } from '../../shared/services/report.services';

@Component({
  selector: 'app-set-report',
  templateUrl: './set.component.html',
  styleUrls: ['./set.component.scss']
})
export class SetReportComponent implements OnChanges {

  @Input() report: any;
  properties: any;
  toggled = [];

  constructor(private reportService: ReportService) {
    this.parseToggled();
  }

  toggleReport(featureReport) {
    let path = featureReport.path;
    let index = this.toggled.indexOf(path);
    if (index > -1) {
      this.toggled.splice(index, 1);
    }
    else {
      this.toggled.push(path)
    }
    this.parseToggled();
  }

  parseToggled() {
    if (this.report) {
      for (let path of this.report.features) {
        let index = this.report.features.indexOf(path);

        if (this.report.reports[index]) {
          if (this.toggled.indexOf(path) > -1) {
            this.report.reports[index].toggle = true;
          }
          else {
            this.report.reports[index].toggle = false;
          }
        }
      }
    }
  }

  ngOnChanges() {
    this.parseToggled();
    if (this.report) {
      if (this.properties) {
        if (this.report.created.$date != this.properties.created) {
          this.updateReport();
        }
        this.addEndTime();
      }
    }
  }

  updateReport() {
    if (this.report) {
      this.properties = Object.assign({}, this.report);
    }
  }

  addEndTime() {
    if (this.report.hasOwnProperty("end")) {
      this.properties.end = this.report.end;
    }
  }

}
