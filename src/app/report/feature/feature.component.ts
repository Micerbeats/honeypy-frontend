import { Component, Input, OnChanges } from '@angular/core';
import { ReportService } from '../../shared/services/report.services';

@Component({
  selector: 'app-feature-report',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.scss']
})
export class FeatureReportComponent implements OnChanges {

  @Input() report: any;
  toggled;
  properties: any = {
    created:undefined
  };

  constructor(private reportService: ReportService) {
    this.parseToggled();

    this.reportService.toggledEvent.subscribe(
      (toggled) => {
        this.toggled = toggled;
        this.parseToggled();
      }
    );
    if (!this.toggled) {
      this.toggled = this.reportService.toggled;
    }
  }

  toggleReport(test) {
    if (test.type == "scenario") {
      this.reportService.toggleScenario(test);
      this.parseToggled();
    }
  }

  parseToggled() {
    if (this.report) {
      for (let test of this.report.tests) {
        let index = this.report.tests.indexOf(test);
        if (test.hasOwnProperty("scenarioId")) {
          let id = this.reportService.getId(test);
          if (this.toggled.indexOf(id) > -1) {
            this.report.tests[index].toggle = true;
          }
          else {
            this.report.tests[index].toggle = false;
          }
        }
      }
    }
  }

  ngOnChanges() {
    this.parseToggled();
    this.toggled = this.reportService.toggled;
    if (this.report) {
      if (this.report.created != this.properties.created) {
        this.updateReport();
      }
      this.addEndTime();
    }
  }

  updateReport() {
    if (this.report) {
      this.properties = Object.assign({}, this.report);
    }
  }

  addEndTime() {
    if (this.report.hasOwnProperty("end")) {
      this.properties.end = this.report.end;
    }
  }

}
