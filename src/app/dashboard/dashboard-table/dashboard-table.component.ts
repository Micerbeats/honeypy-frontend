import { Component, Input } from '@angular/core';
import { PanelService } from '../../shared/services/panel.services';
import { ReportService } from '../../shared/services/report.services';

@Component({
  selector: 'app-dashboard-table',
  templateUrl: './dashboard-table.component.html',
  styleUrls: ['./dashboard-table.component.scss']
})
export class DashboardTableComponent {

  @Input() setReport:any = {};
  @Input() device:any;

  constructor(
    private panelService: PanelService,
    private reportService: ReportService
  ) { }

  getTableItemResult(report) {
    if (report.result === true && report.fail) {
      return {"dashboard-table__table-item-blue":true};
    }
    else if (report.result === true && !report.fail) {
      return {"dashboard-table__table-item-green":true};
    }
    else if (report.result === false && report.fail) {
      return {"dashboard-table__table-item-purple":true};
    }
    else if (report.result === false && !report.fail) {
      return {"dashboard-table__table-item-red":true};
    }
    else if (report.result === undefined) {
      return {"dashboard-table__table-item-grey":true};
    }
  }

  openReport(report, event) {
    let result = this.handleClick(event);
    if (result) {
      this.reportService.setReportId(report.reportId)
      this.reportService.fetchReport();
      this.panelService.openReport();
    }
  }

  onRightClick(report, event) {
    event.preventDefault();
    this.panelService.setContextMenuCoords(event.clientX, event.clientY);
    this.panelService.setContextMenuItem(report);
    this.panelService.toggleContextMenu();
  }

  handleClick(event){
      let result = event.target.classList.contains("dashboard-table__table-item");
      if (result) {
        return true;
      }
      else {
        return false;
      }
  }

}
