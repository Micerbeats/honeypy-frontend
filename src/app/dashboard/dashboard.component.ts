import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute, ParamMap } from "@angular/router";

import { EnvironmentService } from '../shared/services/environment.services';
import { TestService } from '../shared/services/test.services';
import { HostService } from '../shared/services/host.services';
import { HttpService } from '../shared/services/http.services';
import { DashboardService } from '../shared/services/dashboard.services';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  query: any;
  browsers: any;
  hosts: any;
  dashboard:any = [];
  environments:any = [];

  constructor(
    private envService: EnvironmentService,
    private testService: TestService,
    private httpService: HttpService,
    private hostService: HostService,
    private dashboardService: DashboardService,
    private ref :ChangeDetectorRef,
    private router: Router
  ) {
    this.environments = [];
    this.browsers = [];
    this.hosts = [];
    this.browsers = this.testService.browsers;
  }

  ngOnInit() {
    this.initHosts();
    this.initEnvironments();
    this.initQuery();
    this.initDashboard();
  }

  initHosts() {
    /*

      Retrieve the list of hosts from the host service
    */

    this.hostService.hostsEvent.subscribe(
      (hosts) => {
        this.hosts = hosts;
      }
    );

    if (!this.hostService.hosts) {
      this.hostService.getHosts();
    }
    else {
      this.hosts = this.hostService.hosts;
    }
  }

  initEnvironments() {
    /*

      Retrieve the list of environments from the environment service
    */

    this.envService.environmentsEvent.subscribe(
      (environments) => {
        this.environments = environments;
      }
    );

    if (!this.envService.environments) {
      this.envService.fetchEnvironments();
    }
    else {
      this.environments = this.envService.environments;
    }
  }

  initDashboard() {
    /*

      Retrieve the dashboard
    */

    this.dashboardService.dashboardEvent.subscribe(
      (dashboard) => {
        this.dashboard = dashboard;
      }
    );

    this.dashboard = this.dashboardService.dashboard;
  }

  initQuery() {
    /*

      Retrieve any saved query
    */

    this.dashboardService.queryEvent.subscribe(
      (query) => {
        this.query = query;
      }
    );

    if (!this.dashboardService.query) {
      this.dashboardService.getQuery();
    }
    else {
      this.query = this.dashboardService.query;
    }
    this.initSearch(true);
  }

  initSearch(init: boolean = false) {
    if ((this.query.environments.length > 0) && (init)) {
      this.search();
    }
  }

  search() {
    this.dashboardService.search();
  }
}
