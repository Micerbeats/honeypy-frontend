import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardSetComponent } from './dashboard-set.component';

describe('DashboardSetComponent', () => {
  let component: DashboardSetComponent;
  let fixture: ComponentFixture<DashboardSetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardSetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
