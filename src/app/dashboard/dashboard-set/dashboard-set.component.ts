import { Component, Input, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { ReportService } from '../../shared/services/report.services';
import { PanelService } from  '../../shared/services/panel.services';

@Component({
  selector: 'app-dashboard-set',
  templateUrl: './dashboard-set.component.html',
  styleUrls: ['./dashboard-set.component.scss']
})
export class DashboardSetComponent {

  @Input() set: any;
  @Input() index: number;
  getObjectKeys = Object.keys;

  constructor(
    private panelService: PanelService,
    private reportService: ReportService,
    private ref: ElementRef,
    private router: Router
  ) {}

  openReport(report, event) {
    let result = this.handleClick(event);
    if (result) {
      this.reportService.setReportId(report.reportId)
      this.reportService.fetchReport();
      this.panelService.openReport();
    }
  }

  handleClick(event){
      let result = event.target.classList.contains("dashboard-table__table");
      if (result) {
        return true;
      }
      else {
        return false;
      }
  }

  openSet(setName) {
    this.router.navigate(['set', setName]);
  }
}
