import { Component, Input } from '@angular/core';
import { DashboardService } from '../../shared/services/dashboard.services';

@Component({
  selector: 'app-dashboard-environment',
  templateUrl: './dashboard-environment.component.html',
  styleUrls: ['./dashboard-environment.component.scss']
})
export class DashboardEnvironmentComponent {

  query: any;
  getObjectKeys = Object.keys;
  @Input() environment: any;


  constructor(
    private dashboardService: DashboardService
  ) {
    this.initDashboard();
  }

  initDashboard() {
    this.dashboardService.queryEvent.subscribe(
      (query) => {
        this.query = query;
      }
    );

    if (!this.dashboardService.query) {
      this.dashboardService.getQuery();
    }
    else {
      this.query = this.dashboardService.query;
    }
  }

  toggleVisibility() {
    this.environment.visible = !this.environment.visible
  }
}
