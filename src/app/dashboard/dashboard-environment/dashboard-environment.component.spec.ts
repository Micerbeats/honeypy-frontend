import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardEnvironmentComponent } from './dashboard-environment.component';

describe('DashboardEnvironmentComponent', () => {
  let component: DashboardEnvironmentComponent;
  let fixture: ComponentFixture<DashboardEnvironmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardEnvironmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardEnvironmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
