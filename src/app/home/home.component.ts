import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { PanelService } from '../shared/services/panel.services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  host: {'style':'width:100%'}
})

export class HomeComponent {

  menu: any = [
    // {
    //   "label": "Dashboard",
    //   "description": "View and manage large sets of tests"
    // },
    {
      "label": "Create",
      "description": "Create a new feature, phrase or set"
    },
    {
      "label": "Docs",
      "description": "Learn how to use Honeypy"
    },
    {
      "label": "Settings",
      "description": "Configure Honeypy"
    }
  ];

  constructor(private panelService: PanelService, private router: Router) {}

  menuSelect(item) {
    if (item.label == "Create") {
      this.panelService.toggleModal("create");
    }
    else if (item.label == "Docs") {
      this.router.navigateByUrl("/docs/start");
    }
    else if (item.label == "Settings") {
      this.router.navigateByUrl("/settings/hosts");
    }
  }

}
