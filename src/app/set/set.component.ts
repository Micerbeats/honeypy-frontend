import { Component } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { SetService } from '../shared/services/set.services';
import { TestService } from '../shared/services/test.services';
import { ReportService } from '../shared/services/report.services';
import { HttpService } from '../shared/services/http.services';
import { HostService } from '../shared/services/host.services';
import { PanelService } from '../shared/services/panel.services';


@Component({
  selector: 'app-set',
  templateUrl: './set.component.html',
  styleUrls: ['./set.component.scss'],
  host: {
    '(window:keydown)': 'hotkeys($event)'
  }
})
export class SetComponent {

  feature: string = "";
  name: string;
  set: any;

  browsers = [];
  hosts = [];
  setHistory = [];
  options = ["Edit", "Add + / Remove -"];

  file: boolean = false;
  edit: boolean = false;
  modal: boolean = false;
  running: boolean = false;
  addRemove: boolean = false;
  loading: boolean = false;

  history: number;

  constructor(
    private setService: SetService,
    private testService: TestService,
    private reportService: ReportService,
    private httpService: HttpService,
    private panelService: PanelService,
    private hostService: HostService,
    private router: Router
  ) {
    this.browsers = this.testService.browsers;
    this.setService.setEvent.subscribe(
      (set) => {
        this.set = set;
        if (this.set) {
            this.name = this.set.name;
        }
      }
    );

    this.setService.setHistoryEvent.subscribe( (setHistory) => {this.setHistory = setHistory;});

    if (!this.set) {
      this.set = this.setService.set;
      if (this.set) {
          this.name = this.set.name;
      }
    }

    this.running = this.reportService.running;

    this.reportService.runEvent.subscribe( (running) => { this.running = running; } );

    this.hostService.hostsEvent.subscribe( (hosts) => { this.hosts = hosts; } );

    if (this.hosts.length === 0) {
      this.hostService.getHosts();
    }

    this.loading = this.setService.setLoading;
    this.setService.setLoadingEvent.subscribe(
      (loading) => {
        this.loading = loading;
      }
    );

    this.setService.historyRangeEvent.subscribe(
      (history) => {
        this.history = history;
      }
    );

    this.history = this.setService.historyRange;
  }

  setHistoryRange(history) {
    this.setService.setHistoryRange(history);
  }

  hostChange(host) {
    this.set.host == host.name;
  }

  hotkeys(event) {
    if( (event.keyCode == 83 && event.ctrlKey) || (event.keyCode == 83 && event.metaKey)) {
      event.preventDefault();
      if (this.edit) {
        this.save();
      }
    }
    else if (event.key == "Escape") {
      this.toggleAddRemove();
    }
  }

  save() {
    this.httpService.saveSet(this.name, this.set).subscribe(
      (data) => {
        this.toggleForm();
        if (this.name != this.set.name) {
          this.setService.fetchSets();
          this.router.navigate(["set", this.set.name]);
        }
        this.setService.setSet(this.set);
        this.panelService.showAlert("Set Saved");
      },
      (error) => {
        this.panelService.showAlert("Error saving set");
        console.error(error.error);
      }
    );
  }

  toggleForm() {
    this.edit = !this.edit;
  }

  open(feature) {
    let params = {"feature":feature};
    let navigationExtras: NavigationExtras = {
      queryParams: params
    };
    this.router.navigate(['editor'], navigationExtras);
  }

  associate() {
    this.panelService.openModal("associate");
  }

  removeFeature(feature) {
    if (this.addRemove) {
      this.httpService.dissociateSet(this.set.name, [feature]).subscribe(
        (data) => {
          this.panelService.showAlert("Dissociated feature from set");
          this.setService.fetchSet(this.set.name);
        },
        (error) => {
          this.panelService.showAlert(error.error.errors[0].error);
          console.error(error.error);
        }
      );
    }
  }

  run() {
    this.reportService.run(this.set);
  }

  onRightClick(report, event) {
      event.preventDefault();
      this.panelService.setContextMenuCoords(event.clientX, event.clientY);
      this.panelService.setContextMenuItem(report);
      this.panelService.toggleContextMenu();
  }

  select(option) {
    if (option == this.options[0]) {
      this.panelService.setModalFileType('set');
      this.panelService.openModal("edit");
    }
    else if (option == this.options[1]) {
      this.toggleAddRemove();
    }
  }

  toggleAddRemove() {
    this.addRemove = !this.addRemove;
  }

  confirmViewReport(report) {
    if (report.hasOwnProperty("_id") || report.hasOwnProperty("reportId")) {
      if (this.reportService.running === true) {
        let alertResult = confirm("Are you sure you do not want to track the running report?");
        if (alertResult) {
          this.reportService.toggleRunStatus();
          this.viewReport(report);
        }
      }
      else {
        this.viewReport(report);
      }
    }
  }

  viewReport(report) {
    let reportId;
    if (report.hasOwnProperty("_id")) {
      reportId = report["_id"];
    }
    else if (report.hasOwnProperty("reportId")) {
      reportId = report["reportId"];
    }
    if (reportId) {
      this.reportService.setReportId(reportId)
      this.reportService.fetchReport();
      this.panelService.openReport();
    }
  }

  refresh() {
    this.setService.fetchSetHistory();
  }

  displayReportResult(feature) {
    if ( feature.status == "Done" ) {
      return this.getDoneReport(feature);
    }
    else if ( feature.status == "Running" ) {
      return "set-table__reports-cell-running";
    }
    else if ( feature.status == "Queued" ) {
      return "set-table__reports-cell-queued";
    }
    else if ( (feature.status === undefined) && (feature.result === undefined) ) {
      return "set-table__reports-cell-unknown";
    }
    else {
      console.log("Uncaught test result");
    }
  }

  getDoneReport(feature) {
    if ( (feature.result === true) && (feature.fail === false) ) {
      return "set-table__reports-cell-pass";
    }
    else if ( (feature.result === false) && (feature.fail === false) ) {
      return "set-table__reports-cell-fail";
    }
    else if ( (feature.result === false) && (feature.fail === true) ) {
      return "set-table__reports-cell-expected-fail";
    }
    else if ( feature.result === true && (feature.fail === true) ) {
      return "set-table__reports-cell-unexpected-pass";
    }
  }

  searchHistory() {
    if (this.history > 0) {
      this.setHistoryRange(this.history);
      this.setService.fetchSetHistory(undefined, this.history);
    }
  }
}
