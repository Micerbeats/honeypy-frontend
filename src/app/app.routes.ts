import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from './home/home.component';
import { SetComponent } from './set/set.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { OopsComponent } from './oops/oops.component';
import { SettingsComponent } from './settings/settings.component';
import { HostsComponent } from './settings/hosts/hosts.component';
import { HostComponent } from './settings/hosts/host/host.component';
import { EnvironmentsComponent } from './settings/environments/environments.component';
import { EnvironmentComponent } from './settings/environments/environment/environment.component';
import { ReportComponent } from './report/report.component';

// docs
import { DocsComponent } from './docs/docs.component';
import { StepsComponent } from './docs/steps/steps.component';
import { StartComponent } from './docs/start/start.component';
import { AuthComponent } from './auth/auth.component';

import { EditorComponent } from "./editor/editor.component";
import { EditorGuard } from "./shared/guards/editor.guard";
import { AuthGuard } from "./shared/guards/auth.guard";
import { HostGuard } from "./shared/guards/host.guard";
import { SetGuard } from "./shared/guards/set.guard";
import { EnvironmentGuard } from "./shared/guards/environment.guard";
import { TestComponent } from './test/test.component';

const APP_ROUTES: Routes = [
    {
        path: "honeypy/test",
        component: TestComponent,
        canActivate: [
          AuthGuard
        ]
    },
    {
        path: "dashboard",
        component: DashboardComponent,
        canActivate: [
          AuthGuard
        ]
    },
    {
        path: "docs",
        component: DocsComponent,
        canActivate: [
          AuthGuard
        ],
        children: [
          {
            path: 'start',
            component: StartComponent,
          },
          {
            path: 'steps',
            component: StepsComponent
          },
          {
            path: '**',
            component: StartComponent
          }
        ]
    },
    {
        path: "settings",
        component: SettingsComponent,
        canActivate: [
          AuthGuard
        ],
        children: [
          {
            path: 'environments',
            component: EnvironmentsComponent
          },
          {
            path: 'environments/:environment',
            component: EnvironmentComponent,
            canActivate: [
              EnvironmentGuard
            ]
          },
          {
            path: 'hosts',
            component: HostsComponent
          },
          {
            path: 'hosts/new',
            component: HostComponent
          },
          {
            'path': 'hosts/:host',
            component: HostComponent,
            canActivate: [
              HostGuard
            ]
          },
          {
            path: 'hosts/**',
            component: OopsComponent
          }
        ]
    },
    {
        path: "editor",
        component: EditorComponent,
        runGuardsAndResolvers: 'paramsOrQueryParamsChange',
        canActivate: [
          AuthGuard,
          EditorGuard
        ]
    },
    {
        path: 'set/:set',
        component: SetComponent,
        canActivate: [
          AuthGuard,
          SetGuard
        ]
    },
    {
        path: '',
        component: HomeComponent
    },
    {
        path: "**",
        component: OopsComponent,
        canActivate: [
          AuthGuard
        ]
    }
];

export const AppRoutes = RouterModule.forRoot(APP_ROUTES);
