import { Component } from '@angular/core';
import { AuthService } from '../shared/services/auth.services';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent {

  constructor(public authService: AuthService) {}

}
