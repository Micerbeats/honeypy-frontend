import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// routes
import { AppRoutes } from './app.routes';

// services
import { AuthService } from './shared/services/auth.services';
import { PanelService } from './shared/services/panel.services';
import { HttpService } from './shared/services/http.services';
import { TestService } from './shared/services/test.services';
import { ReportService } from './shared/services/report.services';
import { SetService } from './shared/services/set.services';
import { HostService } from './shared/services/host.services';
import { SettingsService } from './shared/services/settings.services';
import { QueueService } from './shared/services/queue.services';
import { EnvironmentService } from './shared/services/environment.services';
import { DashboardService } from './shared/services/dashboard.services';

// main components
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';

import { EditorComponent } from './editor/editor.component';
import { HomeComponent } from './home/home.component';
import { DirectoryComponent } from './directory/directory.component';

// report components
import { ReportComponent } from './report/report.component';
import { FeatureReportComponent } from './report/feature/feature.component';
import { SetReportComponent } from './report/set/set.component';

// library
import { TreeComponent } from './shared/tree/tree.component';
import { ContextMenuComponent } from './shared/context-menu/context.menu';
import { CheckboxDropdownComponent } from './shared/checkbox-dropdown/checkbox-dropdown.component';

// pipes
import { SortPipe } from './shared/pipes/sort';

// guards
import { EditorGuard } from './shared/guards/editor.guard';
import { HostGuard } from './shared/guards/host.guard';
import { SetGuard } from './shared/guards/set.guard';
import { AuthGuard } from './shared/guards/auth.guard';
import { EnvironmentGuard } from './shared/guards/environment.guard';

// Docs
import { DocsComponent } from './docs/docs.component';
import { StepsComponent } from './docs/steps/steps.component';
import { StartComponent } from './docs/start/start.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { TestComponent } from './test/test.component';
import { OopsComponent } from './oops/oops.component';

import { CodemirrorModule } from 'ng2-codemirror';
import { AlertComponent } from './alert/alert.component';
import { SettingsComponent } from './settings/settings.component';
import { HostsComponent } from './settings/hosts/hosts.component';
import { HostComponent } from './settings/hosts/host/host.component';
import { EnvironmentsComponent } from './settings/environments/environments.component';
import { EnvironmentComponent } from './settings/environments/environment/environment.component';

// modal
import { ModalComponent } from './modal/modal.component';
import { NewComponent } from './modal/new/new.component';
import { MoveComponent } from './modal/move/move.component';
import { DeleteComponent } from './modal/delete/delete.component';
import { EditComponent } from './modal/edit/edit.component';
import { SetComponent } from './set/set.component';
import { AuthComponent } from './auth/auth.component';
import { AssociateComponent } from './modal/associate/associate.component';
import { DropdownComponent } from './shared/dropdown/dropdown.component';
import { DetailsComponent } from './modal/details/details.component';
import { QueueComponent } from './modal/queue/queue.component';
import { DashboardEnvironmentComponent } from './dashboard/dashboard-environment/dashboard-environment.component';
import { DashboardTableComponent } from './dashboard/dashboard-table/dashboard-table.component';
import { DashboardSetComponent } from './dashboard/dashboard-set/dashboard-set.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    EditorComponent,
    HomeComponent,
    DirectoryComponent,

    // report components
    ReportComponent,
    FeatureReportComponent,
    SetReportComponent,

    TreeComponent,
    DropdownComponent,
    // ContextMenuComponent,

    SortPipe,

    // modal components
    ModalComponent,
    DashboardComponent,

    // docs
    DocsComponent,
    StepsComponent,

    TestComponent,

    OopsComponent,

    AlertComponent,
    ContextMenuComponent,
    CheckboxDropdownComponent,
    SettingsComponent,
    HostsComponent,
    HostComponent,
    NewComponent,
    MoveComponent,
    DeleteComponent,
    EditComponent,
    SetComponent,
    AuthComponent,
    StartComponent,
    AssociateComponent,
    DetailsComponent,
    QueueComponent,
    EnvironmentsComponent,
    EnvironmentComponent,
    DashboardEnvironmentComponent,
    DashboardTableComponent,
    DashboardSetComponent
  ],
  imports: [
    BrowserModule,
    AppRoutes,
    FormsModule,
    HttpClientModule,
    CodemirrorModule
  ],
  providers: [
    AuthService,
    PanelService,
    HttpService,
    TestService,
    ReportService,
    SetService,
    HostService,
    SettingsService,
    QueueService,
    EnvironmentService,
    DashboardService,
    EditorGuard,
    SetGuard,
    HostGuard,
    EnvironmentGuard,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
