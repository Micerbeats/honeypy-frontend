import { Component, Input, ChangeDetectorRef } from '@angular/core';
import { PanelService } from '../shared/services/panel.services';
import { TestService } from '../shared/services/test.services';
import { SetService } from '../shared/services/set.services';

@Component({
  selector: 'app-directory',
  templateUrl: './directory.component.html',
  styleUrls: ['./directory.component.scss']
})
export class DirectoryComponent {

    features: any;
    phrases: any;
    sets: any;
    private setSearch: any = [];
    private testSearch: any = [];
    private phraseSearch: any = [];
    private previous: any = [];
    private state: string;
    private x: string;
    private y: string;
    private panels: any;
    private editingView: any;
    private contextMenuVisible: boolean = false;
    private visibility: boolean = true;

    selectedTab: any;
    tabs = [
      {
        'label': 'Tests',
        "selector": "tests",
        'active': true
      },
      {
        'label': 'Phrases',
        "selector": "phrases",
        'active': false
      },
      {
        'label': 'Sets',
        "selector": "sets",
        'active': false
      }
    ];

    constructor(private panelService: PanelService, private testService: TestService, private setService: SetService) {
      this.panelService.initToggledFolders();
      this.getActiveDirectory();

      //
      // Panels
      //

      if (!this.panels) {
        this.panels = this.panelService.panels;
      }

      this.panelService.panelsEvent.subscribe(
        (panels) => {
          this.panels = panels;
        }
      );

      //
      // Features directory
      //

      this.testService.featureDirectoryEvent.subscribe(
        (directory) => {
          this.features = directory;
        }
      );

      if (!this.features) {
        this.testService.getFeatures();
      }

      //
      // Phrase directory
      //

      this.testService.phraseDirectoryEvent.subscribe(
        (directory) => {
          this.phrases = directory;
        }
      );

      if (!this.phrases) {
        this.testService.getPhrases();
      }

      //
      // Sets list
      //

      this.setService.setsEvent.subscribe(
        (sets) => {
          this.sets = sets;
        }
      );

      if (!this.sets) {
        this.setService.fetchSets();
      }

    }

    selectTab(tab) {
      this.selectedTab = tab;
      for (var i = 0; i < this.tabs.length; i++) {
        if (this.tabs[i].label == this.selectedTab.label) {
          this.setActiveDirectory(this.selectedTab.label);
          this.tabs[i].active = true;
        }
        else {
          this.tabs[i].active = false;
        }
      }
    }

    setActiveDirectory(directory) {
      localStorage.setItem("activeDirectory", directory);
    }

    getActiveDirectory() {
      let found = false;
      this.selectedTab = localStorage.getItem("activeDirectory");
      for (var i = 0; i < this.tabs.length; i++) {
          if (this.tabs[i].label == this.selectedTab) {
            this.selectedTab = this.tabs[i];
            this.tabs[i].active = true;
            found = true;
          }
          else {
            this.tabs[i].active = false;
          }
      }
      if (found === false) {
        this.selectedTab = this.tabs[0];
        this.tabs[0].active = true;
      }
    }
}
