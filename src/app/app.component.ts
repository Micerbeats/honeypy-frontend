import { Observable } from 'rxjs/Rx';
import { Component } from '@angular/core';
import { AuthService } from "./shared/services/auth.services";
import { PanelService } from "./shared/services/panel.services";
import { TestService } from "./shared/services/test.services";
import { QueueService } from "./shared/services/queue.services";
import { EnvironmentService } from "./shared/services/environment.services";

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  host: {
    '(window:keydown)': 'hotkeys($event)'
  }
})

export class AppComponent {
  title = 'Honeypy';
  panels: any;
  selectedEnvironment: string;

  file: any;
  modalObject: any;

  // context menu
  coordinates = {x:undefined,y:undefined};
  contextMenuVisible: boolean = false;

  constructor(private panelService: PanelService, private testService: TestService, public authService: AuthService, private queueService: QueueService, private envService: EnvironmentService) {
    authService.handleAuthentication();
    if (!this.panels) {
      this.panels = this.panelService.panels;
    }

    this.panelService.panelsEvent.subscribe(
      (panels) => {
        this.panels = panels;
      }
    );

    if (authService.isAuthenticated()) {
      this.openPanels();
      this.initiateContextMenu();
    }
    else {
      this.openAuthPanel();
    }

    if (!this.queueService.queue) {
      this.queueService.fetchQueue();
    }

    this.envService.selectedEnvironmentEvent.subscribe(
      (env) => {
        this.selectedEnvironment = env;
      }
    );

    this.envService.getSelectedEnvironment();
  }

  openAuthPanel() {
    this.panelService.toggleAuth();
  }


  openPanels() {
    this.activateResize();

    this.testService.fileEvent.subscribe(
      (file) => {
        this.file = file;
        this.setModalObject(this.file);
      }
    );
  }

  initiateContextMenu() {
    this.panelService.contextMenuCoordEvent.subscribe(
      (data) => {
        this.coordinates.x = data.x;
        this.coordinates.y = data.y;
      }
    );

    this.panelService.contextMenuEvent.subscribe(
      (data) => {
        this.contextMenuVisible = data;
      }
    );
  }

  changeContextVisibility() {
    this.panelService.toggleContextMenu();
  }

  setModalObject(data) {
    if (data) {
      this.modalObject = data;
    }
  }

  activateResize() {
    $( function() {
      $( ".app__panel-directory" ).resizable({
        handles: "e",
      });
      $( ".app__panel-report" ).resizable({
        handles: "w"
      });
    } );
  }

  hotkeys(event) {
    if ( (event.ctrlKey === true) && (event.key == "/") ) {
      this.panelService.toggleReport();
    }
  }

}
