import { Component } from '@angular/core';
import { PanelService } from '../../shared/services/panel.services';

@Component({
  selector: 'app-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.scss']
})
export class StepsComponent {

  steps: any;
  title: string = "Docs";
  tabs = [
    {
      'label': 'Given',
      'active': true
    },
    {
      'label': 'When',
      'active': false
    },
    {
      'label': 'Then',
      'active': false
    }
  ];
  selectedTab = this.tabs[0];

  constructor(private panelService: PanelService) {
    this.panelService.stepsEvent.subscribe(
      (steps) => {
        this.steps = steps;
        console.log(")))")
        console.log(this.steps)
      }
    );

    if (!this.steps) {
      this.panelService.fetchSteps();
    }
  }

  selectTab(tab) {
    this.selectedTab = tab;
    for (var i = 0; i < this.tabs.length; i++) {
      if (this.tabs[i].label == this.selectedTab.label) {
        this.tabs[i].active = true;
      }
      else {
        this.tabs[i].active = false;
      }
    }
  }
}
