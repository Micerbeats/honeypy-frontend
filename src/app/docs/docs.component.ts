import { Component } from '@angular/core';
import { PanelService } from '../shared/services/panel.services';

@Component({
  selector: 'app-docs',
  templateUrl: './docs.component.html',
  styleUrls: ['./docs.component.scss']
})
export class DocsComponent {

  title: string = "Docs";

  constructor(private panelService: PanelService) {}

}
