import { Component } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent {
  wait: number = 5000;
  visible: boolean = false;
  click: string = "click";
  honeypyInput: string = "";
  elemWaitVisibility: boolean = false;
  elemWaitPresence: boolean = false;

  list: any = [
    {
      "id":1,
      "name":"a",
    },
    {
      "id":2,
      "name":"a",
    },
    {
      "id":3,
      "name":"a",
    },
    {
      "id":4,
      "name":"a",
    },
    {
      "id":5,
      "name":"a",
    },
    {
      "id":6,
      "name":"a",
    },
    {
      "id":7,
      "name":"a",
    },
    {
      "id":8,
      "name":"b",
    },
    {
      "id":9,
      "name":"b",
    },
    {
      "id":10,
      "name":"b",
    }
  ];

  constructor() {}

  toggleClick() {
    if (this.click == "click") {
      this.click = "CLICKED";
    }
    else {
      this.click = "click";
    }
  }

  waitClick() {
    setTimeout( () => {
      this.visible = !this.visible;
    }, this.wait);
  }

  presentAlert() {
    alert("Test Alert");
  }

  waitAlert() {
    setTimeout( () => {
      this.presentAlert();
    }, this.wait);
  }

  waitElementVisibility() {
    setTimeout( () => {
      this.elemWaitVisibility = !this.elemWaitVisibility;
    }, this.wait);
  }

  waitElementPresence() {
    setTimeout( () => {
      this.elemWaitPresence = !this.elemWaitPresence;
    }, this.wait);
  }

}
