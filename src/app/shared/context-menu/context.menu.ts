import {
  Component,
  Input,
  Output,
  EventEmitter,
  ElementRef,
  OnChanges
} from '@angular/core';

import { environment } from "../../../environments/environment";
import { PanelService } from "../services/panel.services";
import { TestService } from "../services/test.services";
import { HttpService } from "../services/http.services";
import { ReportService } from "../services/report.services";
import { SetService } from "../services/set.services";
import { DashboardService } from "../services/dashboard.services";

@Component({
    selector: 'app-contextmenu',
    templateUrl: "./context.menu.html",
    styleUrls: ["./context.menu.scss"],
    host: {
        '(document:click)': 'handleClick($event)',
    }
})

export class ContextMenuComponent implements OnChanges {

    item: any;
    clickedComponent = false;
    @Input() x: any;
    @Input() y: any;
    @Input() set: boolean;
    @Input("visibleInput") visible: any;
    @Input() kind: string;
    @Output() visibleEvent = new EventEmitter();

    style = {
      "left":this.x,
      "top":this.y
    };

    ifDisabled = {
      "menu__item-enabled":true
    };

    constructor (
      private panelService: PanelService,
      private testService: TestService,
      private httpService: HttpService,
      private reportService: ReportService,
      private setService: SetService,
      private dashboardService: DashboardService,
      private elementRef: ElementRef
    ) {
      if (!this.item) {
        this.item = this.panelService.contextMenuItem;
      }

      this.panelService.contextMenuItemEvent.subscribe(
        (item) => {
          this.item = item;
        }
      );
    }

    ngOnChanges() {
      this.y = this.y - 65;
      this.style = {
        "left":this.x + "px",
        "top":this.y + "px"
      };

    }

    handleClick(event){
        var clickedComponent = event.target;
        var inside = false;
        do {
            if (clickedComponent === this.elementRef.nativeElement) {
                inside = true;
            }
            clickedComponent = clickedComponent.parentNode;
        } while (clickedComponent);
        if(!inside){
          if (this.visible === true) {
            this.hide();
          }
        }
    }

    hide() {
      this.visible = false;
      this.visibleEvent.emit(this.visible);
    }

    new() {
      this.hide();
      this.panelService.setModalItem(this.item);
      this.panelService.openModal("create");
    }

    newTab() {
      if (!this.item.hasOwnProperty('kind')) {
        this.item["kind"] = "feature";
      }
      let url = "";
      url = environment.url + "/editor?" + this.item.kind + "=" + this.item.path;
      this.hide();
      var win = window.open(url, '_blank');
      win.focus();
    }

    associate() {
      // Associate feature to set
      if (this.item) {
        this.hide();
        this.panelService.setModalItem(this.item);
        this.panelService.openModal("associate");
      }
    }

    copy() {
      if (this.item) {
        this.hide();
        this.panelService.setModalItem(this.item);
        this.panelService.openModal("copy");
      }
    }

    moveRename() {
      if (this.item) {
        this.hide();
        this.panelService.setModalItem(this.item);
        this.panelService.openModal("move");
      }
    }

    delete() {
      if (this.item) {
        this.hide();
        this.panelService.setModalItem(this.item);
        this.panelService.openModal("delete");
      }
    }

    rerun() {
      this.item.kind = 'feature';
      this.httpService.run(this.item, true).subscribe(
        (data) => {
          this.panelService.showAlert("Rerunning " + this.item.name);
          this.trackReport(data.id);
          this.hide();
          this.panelService.openReport();
          this.refresh();
        },
        (error) => {
          this.panelService.showAlert(error.errors[0]);
        }
      );
    }

    trackReport(reportId) {
      if (this.reportService.running === true) {
        let result = confirm("Do you want to switch the tracked report to " + this.item.name + "?");
        if (result) {
          this.reportService.setReportId(reportId);
          this.reportService.fetchReport();
        }
      }
      else {
        this.reportService.toggleRunStatus();
        this.reportService.setReportId(reportId);
        this.reportService.fetchReport();
      }
    }

    finish() {
      this.hide();
      this.httpService.status(this.item["reportId"], "finish", this.item["path"]).subscribe(
        (data) => {
          this.panelService.showAlert("Force Finished the Report");
          this.refresh();
        },
        (error) => {
          this.panelService.showAlert("Unable to Force Finish the Specified Report");
        }
      );
    }

    refresh() {
      if (this.setService.set) {
        this.setService.fetchSet();
      }
      else {
        this.dashboardService.search();
      }
    }
}
