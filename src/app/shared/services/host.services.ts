import { Injectable, EventEmitter } from '@angular/core';
import { HttpService } from './http.services';

@Injectable()
export class HostService {

  host: any;
  hosts: any;
  hostEvent: EventEmitter<any> = new EventEmitter();
  hostsEvent: EventEmitter<any> = new EventEmitter();

  constructor(private httpService: HttpService) {}

  getHosts() {
    this.httpService.getHosts().subscribe(
      (data) => {
        this.setHosts(data);
      },
      (error) => {
        console.log("Error getting hosts");
      }
    );
  }

  setHosts(hosts) {
    this.hosts = hosts;
    this.hostsEvent.emit(this.hosts);
  }

  filterActiveHosts(hosts) {
    this.hosts = [];
    for (let host of hosts) {
      if (host.active === true) {
        this.hosts.push(host);
      }
    }
  }

  setHost(host) {
    this.host = host;
    this.hostEvent.emit(this.host);
  }

}
