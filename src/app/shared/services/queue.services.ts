import { Injectable, EventEmitter } from '@angular/core';
import { HttpService } from './http.services';
import { PanelService } from './panel.services';
import { environment } from '../../../environments/environment';

@Injectable()
export class QueueService {

  queue: any;
  queueEvent: EventEmitter<any> = new EventEmitter();

  constructor(
    private httpService: HttpService,
    private panelService: PanelService,
  ) {}

  fetchQueue() {
    let queue = localStorage.getItem("queue");
    if (!queue) {
      queue = environment.defaultQueue;
    }
    this.setQueue(queue);
  }

  setQueue(queue) {
    this.queue = queue;
    localStorage.setItem("queue", this.queue);
    this.queueEvent.emit(this.queue);
  }

}
