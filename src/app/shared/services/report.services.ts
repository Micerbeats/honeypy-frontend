import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpService } from './http.services';
import { SetService } from './set.services';
import { PanelService } from './panel.services';
import { TestService } from './test.services';
import { EnvironmentService } from './environment.services';
import { QueueService } from './queue.services';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Injectable()
export class ReportService {

  // selected environment
  selectedEnvironment: string;

  // report
  report: any;
  reportEvent: EventEmitter<any> = new EventEmitter();

  // running
  running: boolean = false;
  runEvent: EventEmitter<any> = new EventEmitter();

  // report id
  reportId: string;
  reportIdEvent: EventEmitter<any> = new EventEmitter();

  toggled: any = [];
  toggledEvent: EventEmitter<any> = new EventEmitter();

  file: any;
  set: any;

  constructor(
    private queueService: QueueService,
    private httpService: HttpService,
    private setService: SetService,
    private panelService: PanelService,
    private testService: TestService,
    private envService: EnvironmentService,
    private router: Router,
    activatedRoute: ActivatedRoute) {

    this.testService.fileEvent.subscribe(
      (file) => {
        this.file = file;
      }
    );

    this.setService.setEvent.subscribe(
      (set) => {
        this.set = set;
      }
    );

    this.envService.selectedEnvironmentEvent.subscribe(
      (environment) => {
        this.selectedEnvironment = environment;
      }
    );

    if (!this.envService.selectedEnvironment) {
      this.envService.getSelectedEnvironment();
    }
    else {
      this.selectedEnvironment = this.envService.selectedEnvironment;
    }


  }

  setQueue(data) {
    data["queue"] = this.queueService.queue;
    return data;
  }

  run(data) {
    if (data["kind"] == "feature") {
      data = this.setQueue(data);
      this.toggleRunStatus();
      this.runFeature(data);
    }
    else if (data["kind"] == "set") {
      data = this.setQueue(data);
      this.toggleRunStatus();
      this.runSet(data);
    }
  }

  runFeature(file) {
    file["environment"] = this.selectedEnvironment;
    this.httpService.run(file, undefined).subscribe(
      (data) => {
        this.panelService.showAlert("Running Feature");
        this.panelService.openReport();
        this.setReportId(data.id);
        this.fetchReport();
      },
      (errors) => {
        this.panelService.showAlert("Error Running Feature");
        console.error(errors.error);
        this.toggleRunStatus();
      }
    );
  }

  runSet(set) {
    set["environment"] = this.selectedEnvironment;
    this.httpService.run(set, undefined).subscribe(
      (data) => {
        this.panelService.showAlert("Running Set");
        this.panelService.openReport();
        this.setReportId(data.id);
        this.fetchReport();
        this.setService.fetchSetHistory();
      },
      (errors) => {
        this.panelService.showAlert("Error Running Set");
        console.error(errors.error);
        this.toggleRunStatus();
      }
    );
  }

  setReport(report) {
    this.report = report;
    this.reportEvent.emit(this.report);
  }

  setReportId(reportId) {
    this.reportId = reportId;
    this.reportIdEvent.emit(this.reportId);
  }

  fetchReport() {
    this.httpService.getReport(this.reportId).subscribe(
      (data) => {
        this.setReport(data);
      },
      (error) => {
        this.panelService.showAlert("Error Fetching the Report");
      }
    );
  }

  toggleRunStatus() {
    this.running = !this.running;
    if (this.running) {
      this.toggled = [];
    }
    this.runEvent.emit(this.running);
  }

  getId(test) {
    if (test.scenarioId.hasOwnProperty("$oid")) {
      return test.scenarioId.$oid;
    }
    else {
      return test.scenarioId;
    }
  }

  toggleScenario(scenario) {
    let id = this.getId(scenario);
    let index = this.toggled.indexOf(id);
    if (index > -1) {
      this.toggled.splice(index, 1);
    }
    else {
      this.toggled.push(id)
    }
    this.toggledEvent.emit(this.toggled);
  }

}
