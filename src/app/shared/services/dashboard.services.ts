import { Injectable, EventEmitter } from '@angular/core';
import { HttpService } from './http.services';
import { TestService } from './test.services';
import { PanelService } from './panel.services';

@Injectable()
export class DashboardService {

  dashboard: any = [];
  dashboardEvent: EventEmitter<any> = new EventEmitter();

  query: any;
  queryEvent: EventEmitter<any> = new EventEmitter();

  constructor(private httpService: HttpService, private testService: TestService, private panelService: PanelService) {}

  setQuery(query) {
    this.query = query;
    localStorage.setItem("query", JSON.stringify(this.query));
    this.queryEvent.emit(this.query);
  }

  getQuery() {
    let jsonString = localStorage.getItem("query")
    let query;
    if (jsonString) {
      query = JSON.parse(jsonString);
    }
    if (!query) {
      query = {
        browsers: this.testService.getBrowsers(),
        environments: [],
        hosts: [],
        history:2,
        kind: "set"
      }
    }
    this.setQuery(query);
  }

  search() {
    this.dashboard = [];
    let environments = Object.assign([], this.query["environments"]);
    for (let environment of environments) {
      let query = Object.assign({}, this.query);
      delete query["environments"];
      query["environment"] = environment;
      this.fetchDashboard(query);
    }

  }

  fetchDashboard(query) {
    this.httpService.getDashboard(query).subscribe(
      (data) => {
        let environment = {name:query["environment"], reports:data, browsers:Object.assign([], query.browsers)}
        this.dashboard.push(environment);
        this.dashboardEvent.emit(this.dashboard);
      },
      (errors) => {

      }
    );
  }

  setDashboard(dashboard) {
    this.dashboard = dashboard;
    this.dashboardEvent.emit(this.dashboard);
  }

}
