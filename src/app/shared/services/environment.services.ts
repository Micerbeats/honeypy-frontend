import { Injectable, EventEmitter } from '@angular/core';
import { HttpService } from './http.services';
import { PanelService } from './panel.services';

@Injectable()
export class EnvironmentService {

  environment: any;
  environmentEvent: EventEmitter<any> = new EventEmitter();

  environments: any;
  environmentsEvent: EventEmitter<any> = new EventEmitter();

  selectedEnvironment: any;
  selectedEnvironmentEvent: EventEmitter<any> = new EventEmitter();

  constructor(
    private httpService: HttpService,
    private panelService: PanelService,
  ) {}

  fetchEnvironments() {
    this.httpService.getEnvironments().subscribe(
      (data) => {
        this.setEnvironments(data);
      },
      (errors) => {
        this.panelService.showAlert("Error Retrieving Environments");
        console.error(errors.error);
      }
    );
  }

  setEnvironments(envs) {
    this.environments = envs;
    this.environmentsEvent.emit(this.environments);
  }

  getEnvironment(env) {
    this.httpService.getEnvironment(env).subscribe(
      (data) => {
        this.setEnvironment(data);
      },
      (errors) => {
        this.panelService.showAlert("Error Retrieving Environment " + env);
        console.error(errors.error);
      }
    );
  }

  setEnvironment(env) {
    this.environment = env;
    this.environmentEvent.emit(this.environment);
  }

  selectEnvironment(environment) {
    this.selectedEnvironment = environment;
    localStorage.setItem("selectedEnvironment", this.selectedEnvironment);
    this.selectedEnvironmentEvent.emit(this.selectedEnvironment);
  }

  getSelectedEnvironment() {
    let selectedEnvironment = localStorage.getItem("selectedEnvironment");
    this.selectEnvironment(selectedEnvironment)
  }

}
