import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpService } from './http.services';
import { PanelService } from './panel.services';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Injectable()
export class SetService {

  // search filters
  page = 1;
  limit = 30;

  // single set
  set: any;
  setEvent: EventEmitter<any> = new EventEmitter();

  // list of sets
  sets: any;
  setsEvent: EventEmitter<any> = new EventEmitter();

  // set reports
  setHistory: any;
  setHistoryEvent: EventEmitter<any> = new EventEmitter();

  // set history loading
  setLoading: boolean = false;
  setLoadingEvent: EventEmitter<any> = new EventEmitter();

  // set history range
  historyRange: number;
  historyRangeEvent: EventEmitter<any> = new EventEmitter();

  constructor(private panelService: PanelService, private httpService: HttpService, private router: Router, activatedRoute: ActivatedRoute) {}

  getHistory() {
    let history = Number(localStorage.getItem("history"));
    if (!history) {
      history = 7;
    }
    this.setHistoryRange(history);
  }

  setHistoryRange(history) {
    localStorage.setItem("history", history);
    this.historyRange = history;
    this.historyRangeEvent.emit(this.historyRange);
  }

  fetchSet(set = this.set.name) {
    this.httpService.getSet(set).subscribe(
      (data) => {
        this.setSet(data);
      },
      (error) => {
        this.panelService.showAlert("Error retrieving set");
        console.error(error);
      }
    );
  }

  fetchSetHistory(query = undefined, history = this.historyRange) {
    let min = this.getMinHistory(history);
    if ( (!query) && (this.set) ) {
      query = {"kind":"set", "name":this.set.name, "created": {"min":min}}
    }
    this.toggleLoading(true);
    let payload = {
      "pagination": {
        "page":this.page,
        "limit":this.limit
      },
      "search": query
    };
    this.httpService.searchReports(payload).subscribe(
      (reports) => {
        this.toggleLoading(false);
        this.setHistory = this.parseHistory(reports.results);
        this.setHistoryEvent.emit(this.setHistory);
      },
      (error) => {
        this.toggleLoading(false);
        this.panelService.showAlert("Error retrieving set history");
        console.error(error);
      }
    );
  }

  getMinHistory(history) {
    let duration = history * 86400000;
    return Date.now() - duration;
  }

  parseHistory(data) {
    let history = [];
    for (let i = 0; i < data.length; i++) {
      let report = data[i];
      let filtered_report = this.filterFeatureReports(report);
      let final_report = this.matchReport(filtered_report);
      history.push(final_report);
    }
    return history;
  }

  filterFeatureReports(report) {
    if (report) {
      let i = report.reports.length;
      if (i > 0) {
        let reverse = report.reports.reverse();
        while (i--) {
          if (reverse[i]) {
            if (this.set.features.indexOf(reverse[i].path) == -1) {
              reverse.splice(i, 1);
            }
          }
        }
        report.reports = reverse.reverse();
      }
    }
    return report;
  }

  matchReport(report) {
    for (let i = 0; i < this.set.features.length; i++) {
      let feature = this.set.features[i];
      let found = false;
      for (var x = 0; x < report.reports.length; x++) {
        let feature_report = report.reports[x];
        if (feature_report) {
            if (feature_report.path == feature) {
              found = true;
              break;
            }
        }
      }
      if (!found) {
        report.reports.splice(i, 1, {"kind":"feature", "path":feature, "result":undefined, "message":undefined, "status":undefined});
      }
    }
    return report;
  }

  setSet(set) {
    this.set = set;
    this.setEvent.emit(this.set);
    this.getHistory();
    this.fetchSetHistory();
  }

  fetchSets() {
    this.httpService.getSets().subscribe(
      (data) => {
        this.setSets(data);
      },
      (error) => {
        this.panelService.showAlert("Error retrieving sets");
        console.error(error);
      }
    );
  }

  setSets(sets) {
    this.sets = sets;
    this.setsEvent.emit(this.sets);
  }

  toggleLoading(value = undefined) {
    if (value === undefined) {
      this.setLoading = !this.setLoading;
    }
    else {
        this.setLoading = value;
    }
    this.setLoadingEvent.emit(this.setLoading);
  }
}
