import { Injectable, EventEmitter } from '@angular/core';
import { HttpService } from './http.services';

@Injectable()
export class SettingsService {

  ifAdd: boolean = false;
  ifAddEvent: EventEmitter<any> = new EventEmitter();

  title: any;
  titleEvent: EventEmitter<any> = new EventEmitter();

  header: string;
  headerEvent: EventEmitter<any> = new EventEmitter();

  constructor(private httpService: HttpService) {}

  setTitle(title) {
    this.title = title;
    this.titleEvent.emit(this.title);
  }

  setHeader(header) {
    this.header = header;
    this.headerEvent.emit(this.header);
  }

  openAdd() {
    this.ifAdd = true;
    this.ifAddEvent.emit(this.ifAdd);
  }

  closeAdd() {
    this.ifAdd = false;
    this.ifAddEvent.emit(this.ifAdd);
  }

  toggleAdd() {
    if (this.ifAdd) {
      this.openAdd();
    }
    else {
      this.closeAdd();
    }
  }
}
