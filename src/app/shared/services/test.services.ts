import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpService } from './http.services';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { PanelService } from './panel.services';

@Injectable()
export class TestService {

  phrases: any;
  phraseDirectoryEvent: EventEmitter<any> = new EventEmitter();

  features: any;
  featureDirectoryEvent: EventEmitter<any> = new EventEmitter();

  file: any;
  fileEvent: EventEmitter<any> = new EventEmitter();

  browsers = [
    {"name":"chrome"},
    {"name":"firefox"},
    {"name":"safari"},
    {"name":"edge"},
    {"name":"ie11"}
  ];

  constructor(private httpService: HttpService, private router: Router, activatedRoute: ActivatedRoute, private panelService: PanelService) {}

  getBrowsers() {
    let browsers = [];
    for (let browser of this.browsers) {
      browsers.push(browser.name);
    }
    return browsers;
  }

  fetchFile(type, path) {
    this.httpService.getFile(type, path).subscribe(
        (data: any) => {
          this.setFile(data);
          return true;
        },

        (error: any) => {
          this.setFile(undefined);
          return false;
        }
    );
  }

  ifFile(path) {
    if (path.match(/\.(feature|phrase)$/)) {
      return true;
    }
    else {
      return false;
    }
  }

  setFile(file) {
    this.file = file;
    this.panelService.setModalItem(this.file);
    this.fileEvent.emit(this.file);
  }

  saveFile(file) {
    this.httpService.saveFile(file).subscribe(
      (data) => {
        this.panelService.setAlertMessage("Saved");
        this.panelService.toggleAlert();
        this.setFile(file);
      },
      (error) => {
        this.panelService.setAlertMessage("Error saving file");
        this.panelService.toggleAlert();
      }
    );
  }

  // get feature directory
  getFeatures() {
    this.httpService.getDirectory("feature").subscribe(
      (data) => {
        this.features = data.children;
        this.featureDirectoryEvent.emit(this.features);
      },
      (error) => {console.log(error)}
    );
  }

  // get phrase directory
  getPhrases() {
    this.httpService.getDirectory("phrase").subscribe(
      (data) => {
        this.phrases = data.children;
        this.phraseDirectoryEvent.emit(this.phrases);
      },
      (error) => {}
    );
  }

}
