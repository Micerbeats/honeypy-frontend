import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpService } from './http.services';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Injectable()
export class PanelService {

  panels: any;
  panelsEvent: EventEmitter<any> = new EventEmitter();

  // expanded report
  expandedReport: boolean = false;

  //
  steps: any;
  stepsEvent: EventEmitter<any> = new EventEmitter();

  // modal item
  modalItem: any;
  modalItemEvent: EventEmitter<any> = new EventEmitter();

  // toggled folders
  toggledFoldersEvent: EventEmitter<any> = new EventEmitter();

  // context menu
  contextMenuItem: any;
  contextMenuItemEvent: EventEmitter<any> = new EventEmitter();
  contextMenuVisible: boolean = false;
  contextMenuEvent: EventEmitter<any> = new EventEmitter();
  contextMenuCoords = {
    'x':undefined,
    'y':undefined
  };
  contextMenuCoordEvent: EventEmitter<any> = new EventEmitter();

  // Active dropdown
  private activeDropdown: string;
  dropdownEvent: EventEmitter<any> = new EventEmitter();

  constructor(private httpService: HttpService, private router: Router, private activatedRoute: ActivatedRoute) {
    if (!this.panels) {
      this.panels = {
        "directory": {
          "class": {
            "app__panel": true,
            "app__panel-directory": true
          }
        },
        "auth": {
          "class": {
            "app__panel-auth": true,
            "app__panel": true,
            "app__panel-auth-exit": true,
            "app__panel-auth-enter": false
          }
        },
        "main": {
          "class": {
            "app__panel": true,
            "app__panel-main": true
          }
        },
        "report": {
          "visibility": false,
          "class": {
            "app__panel": true,
            "app__panel-report": true,
            "app__panel-report-exit": true,
            "app__panel-report-enter": false
          }
        },
        "modal": {
          "type": "create",
          "kind": "feature",
          "menu": {},
          "class": {
            "app__modal": true,
            "app__modal-exit": true,
            "app__modal-enter": false
          }
        },
        "alert": {
          "message": "",
          "class": {
            "app__alert": true,
            "app__alert-exit": true,
            "app__alert-enter": false
          }
        }
      }
    };
    this.panelsEvent.emit(this.panels);
  }

  toggleAuth() {
    let timer = Observable.timer(500);
    this.closeAuth();
    timer.subscribe(
      (t) => {
        this.openAuth();
      }
    );
  }

  openAuth() {
    this.panels.auth.class["app__panel-auth-exit"] = false;
    this.panels.auth.class["app__panel-auth-enter"] = true;
    this.panelsEvent.emit(this.panels);
  }

  closeAuth() {
    this.panels.auth.class["app__panel-auth-exit"] = true;
    this.panels.auth.class["app__panel-auth-enter"] = false;
    this.panelsEvent.emit(this.panels);
  }

  // report panel
  openReport() {
    if (this.expandedReport === false) {
      this.panels.report.visibility = true;
      this.panels.report.class["app__panel-report-exit"] = false;
      this.panels.report.class["app__panel-report-enter"] = true;
    }
  }

  closeReport() {
    this.panels.report.visibility = false;
    this.panels.report.class["app__panel-report-exit"] = true;
    this.panels.report.class["app__panel-report-enter"] = false;
    this.panelsEvent.emit(this.panels);
  }

  toggleReport() {
    if (this.panels.report.visibility) {
      this.closeReport();
    }
    else {
      this.openReport();
    }
  }

  // modal view
  openModal(type) {
    this.panels.modal.type = type;
    this.panels.modal.class["app__modal-exit"] = false;
    this.panels.modal.class["app__modal-enter"] = true;
    this.panelsEvent.emit(this.panels);
  }

  closeModal() {
    this.panels.modal.class["app__modal-exit"] = true;
    this.panels.modal.class["app__modal-enter"] = false;
    this.panelsEvent.emit(this.panels);
  }

  toggleModal(type) {
    if (this.panels.modal.class["app__modal-enter"] === false) {
      this.openModal(type);
    }
    else {
      this.closeModal();
    }
  }

  setModalFileType(kind) {
    this.panels.modal.kind = kind;
    this.panelsEvent.emit(this.panels);
  }

  //
  // alerts
  //

  showAlert(message) {
    this.setAlertMessage(message);
    this.toggleAlert();
  }

  setAlertMessage(message) {
    this.panels.alert.message = message;
    this.panelsEvent.emit(this.panels);
  }

  toggleAlert() {
    let timer = Observable.timer(4000);
    this.openAlert();
    timer.subscribe(
      (t) => {
        this.closeAlert();
      }
    );
  }

  openAlert() {
    this.panels.alert.class["app__alert-exit"] = false;
    this.panels.alert.class["app__alert-enter"] = true;
    this.panelsEvent.emit(this.panels);
  }

  closeAlert() {
    this.panels.alert.class["app__alert-exit"] = true;
    this.panels.alert.class["app__alert-enter"] = false;
    this.panelsEvent.emit(this.panels);
  }

  //
  // modals
  //

  setModalItem(item) {
    this.modalItem = item;
    this.modalItemEvent.emit(this.modalItem);
  }

  setModalType(panel) {
    this.panels["modal"]["type"] = panel;
    this.panelsEvent.emit(this.panels);
  }

  //
  // context menu
  //

  setContextMenuItem(item) {
    this.contextMenuItem = item;
    this.contextMenuItemEvent.emit(this.contextMenuItem);
  }

  setContextMenuCoords(x, y) {
    this.contextMenuCoords.x = x;
    this.contextMenuCoords.y = y;
    this.contextMenuCoordEvent.emit(this.contextMenuCoords);
  }

  toggleContextMenu() {
      this.contextMenuVisible = !this.contextMenuVisible;
      this.contextMenuEvent.emit(this.contextMenuVisible)
  }

  //
  // docs
  //

  setSteps(steps) {
    this.steps = steps;
    this.stepsEvent.emit(this.steps);
  }

  fetchSteps() {
    this.httpService.getSteps().subscribe(
      (steps) => {
        this.setSteps(steps);
      },
      (error) => {
        this.showAlert("Error Retrieving Steps");
        console.error(error);
      }
    );
  }

  //
  // Tree
  //

  initToggledFolders() {
    let toggledFolders = this.getToggledFolders();
    if (!toggledFolders) {
      this.setToggledFolders({"feature":[],"phrase":[]});
    }
  }

  getToggledFolders() {
    return JSON.parse(localStorage.getItem("toggledFolders"));
  }

  setToggledFolders(toggledFolders) {
    localStorage.setItem("toggledFolders", JSON.stringify(toggledFolders));
    this.toggledFoldersEvent.emit(toggledFolders);
  }

  expandFolder(item) {
    let toggledFolders = this.getToggledFolders();
    if (toggledFolders[item.kind].indexOf(item.path) == -1) {
      toggledFolders[item.kind].push(item.path)
      this.setToggledFolders(toggledFolders);
    }
  }

  collapseFolder(item) {
    let toggledFolders = this.getToggledFolders();
    let index = toggledFolders[item.kind].indexOf(item.path)
    if (index > -1) {
      toggledFolders[item.kind].splice(index, 1);
      this.setToggledFolders(toggledFolders);
    }
  }
}
