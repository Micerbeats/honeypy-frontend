
import { Observable } from "rxjs/Rx";
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from "@angular/common/http";
import { environment } from '../../../environments/environment';

@Injectable()
export class HttpService {

  constructor(private http: HttpClient) {}

  cleanPayload(data) {
    delete data._etag;
    delete data.created;
    delete data.modified;
    delete data._links;
    delete data._id;
    return data;
  }

  addAuth(headers) {
    headers = headers.set("Authorization", "Basic " + btoa(environment.username + ":" + environment.password));
    return headers;
  }

  //
  // Test Service
  //

  getFile(kind: string, path: string) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    let parameters = new HttpParams().set('path', path).set('kind', kind);
    return this.http.get(environment.testEnv, {params: parameters, headers:headers})
  }

  createFile(data) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.post(environment.testEnv, data, {headers:headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  saveFile(data) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    delete data["modified"];
    delete data["created"];
    return this.http.patch(environment.testEnv, data, {headers:headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  copyFile(data) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.post(environment.testEnv + "/copy", data, {headers:headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  moveFile(data) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.post(environment.testEnv + "/move", data, {headers:headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  deleteFile(kind, path) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    let parameters = new HttpParams().set('path', path).set('kind', kind);
    return this.http.delete(environment.testEnv, {params:parameters, headers:headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  getDirectory(kind: string) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    let parameters = new HttpParams().set('kind', kind);
    return this.http.get( environment.testEnv + '/all', {params:parameters, headers:headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  getSteps() {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.get( environment.testEnv + '/steps', {headers:headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  run(file: any, rerun) {
    let parameters;
    if (rerun) {
      parameters = new HttpParams().set('rerun', "true");
    }
    else {
      parameters = new HttpParams();
    }
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.post( environment.testEnv + '/run', file, {headers:headers,params: parameters})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  status(reportId, type, path) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    let parameters = new HttpParams().set('type', type).set('path', path);
    return this.http.get( environment.reportEnv + '/' + reportId + '/status', {headers:headers,params: parameters})
      .map((data: Response) => data)
      .catch(this.handleError);
  }


  //
  // Host Service
  //

  getHosts() {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.get(environment.hostEnv, {headers:headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  getHost(name) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.get(environment.hostEnv + "/" + name, {headers:headers})
  }

  createHost(data) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.post(environment.hostEnv, data, {headers:headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  saveHost(name, data) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    data = this.cleanPayload(data);
    return this.http.patch(environment.hostEnv + "/" + name, data, {headers:headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  deleteHost(name) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.delete(environment.hostEnv + "/" + name, {headers:headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  //
  // Report Service
  //

  getReport(reportId, deep = "true") {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    let parameters = {};
    if (deep) {
      parameters = {'deep':deep};
    }
    return this.http.get(environment.reportEnv + "/" + reportId, {headers:headers, params: parameters})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  searchReports(query) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.post(environment.reportEnv + "/search", query, {headers:headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  handleError(error: any) {
    return Observable.throw(error);
  }

  //
  // Set Service
  //

  createSet(set) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.post(environment.setEnv, set, {headers: headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  getSet(set) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.get(environment.setEnv + "/" + set, {headers: headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  saveSet(name, set) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.patch(environment.setEnv + "/" + name, set, {headers: headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  associateSet(name, features) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    let payload = {"features": features};
    return this.http.post(environment.setEnv + "/" + name + "/associate", payload, {headers: headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  dissociateSet(name, features) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    let payload = {"features": features};
    return this.http.post(environment.setEnv + "/" + name + "/dissociate", payload, {headers: headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  getSets() {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.get(environment.setEnv, {headers: headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  deleteSet(set) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.delete(environment.setEnv + "/" + set, {headers: headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  //
  // Environment Service
  //

  createEnvironment(data) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.post(environment.envEnv, data, {headers: headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  getEnvironments() {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.get(environment.envEnv, {headers: headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  getEnvironment(env) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.get(environment.envEnv + "/" + env, {headers: headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  addVariable(env, name, value) {
    let payload = {
      variables: [{
        name:name,
        value:value
      }]
    };
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.patch(environment.envEnv + "/" + env + "/add", payload, {headers: headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  removeVariable(env, variable) {
    let payload = {
      variables: [variable]
    };
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.patch(environment.envEnv + "/" + env + "/remove", payload, {headers: headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  deleteEnvironment(env) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    return this.http.delete(environment.envEnv + "/" + env, {headers: headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  getDashboard(query) {
    let headers = new HttpHeaders();
    headers = this.addAuth(headers);
    let min = this.getMinHistory(query.history);
    query.created = {"min":min};
    return this.http.post(environment.reportEnv + "/dashboard", query, {headers: headers})
      .map((data: Response) => data)
      .catch(this.handleError);
  }


  //
  // Giphy
  //

  getGif(search) {
    let parameters = new HttpParams().set('api_key', "0VUc4fqmvPIlB27DXSyIipllSmgy9OQL").set('q', search).set('limit', '50').set('rating', "g");
    return this.http.get("http://api.giphy.com/v1/gifs/search", {params:parameters})
      .map((data: Response) => data)
      .catch(this.handleError);
  }

  //
  // Helpers
  //

  getMinHistory(history) {
    let duration = history * 86400000;
    return Date.now() - duration;
  }
}
