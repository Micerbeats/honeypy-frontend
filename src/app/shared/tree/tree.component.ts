import { Component, Input, Output, OnInit, OnChanges, EventEmitter } from '@angular/core';
import { PanelService } from '../services/panel.services';
import { TestService } from '../services/test.services';
import { SetService } from '../services/set.services';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'tree',
  templateUrl: "./tree.component.html",
  styleUrls: ["./tree.component.scss"]
})

export class TreeComponent implements OnInit, OnChanges {
  @Input("items") items: any = [];
  private toggled: any = {
    'feature': [],
    'phrase': []
  };

  constructor(
    private router: Router,
    private panelService: PanelService,
    private testService: TestService,
    private setService: SetService
  ) {
    this.panelService.toggledFoldersEvent.subscribe(
      (folders) => {
        this.toggled = folders;
        this.parseToggled();
      }
    );
  }

  ngOnInit() {
    this.toggled = this.panelService.getToggledFolders();
    this.parseToggled();
  }

  ngOnChanges() {
    this.parseToggled();
  }

  parseToggled() {
    if (this.items) {
      for (let item of this.items) {
        let index = this.items.indexOf(item);
        if (this.toggled[item.kind]) {
          if (this.toggled[item.kind].indexOf(item.path) == -1) {
            this.items[index].toggle = false;
          }
          else {
            this.items[index].toggle = true;
          }
        }
      }
    }
  }

  onRightClick(item, event) {
    event.preventDefault();
    this.panelService.setContextMenuCoords(event.clientX, event.clientY);
    this.panelService.setContextMenuItem(item);
    this.panelService.toggleContextMenu();
  }

  clickNode(item, event) {
    if (item.type == "folder") {
      if (!item.toggle) {
        this.panelService.expandFolder(item);
      }
      else {
        this.panelService.collapseFolder(item);
      }
    }
    else {
      if (item.type == "file") {
        if (event.metaKey === true) {
          let url = "";
            url = "http://localhost:4200/editor?" + item.type + "=" + item.path;
          var win = window.open(url, '_blank');
          win.focus();
        }
        else {
            this.openFile(item);
        }
      }
      else if (item.kind == "set") {
        this.openSet(item);
      }
    }
  }

  openFile(item) {
    let params = {};
    params[item.kind] = item.path;
    let navigationExtras: NavigationExtras = {
      queryParams: params
    };
    this.router.navigate(['editor'], navigationExtras);
    if (this.setService.set) {
      this.setService.setSet(undefined);
    }
  }

  openSet(set) {
    if (this.testService.file) {
      this.testService.setFile(undefined);
    }
    this.router.navigate(['set', set.name]);
  }
}
