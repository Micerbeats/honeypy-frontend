import {
  Component,
  Input,
  Output,
  EventEmitter,
  ElementRef
} from '@angular/core';

@Component({
    selector: 'app-dropdown',
    templateUrl: "./dropdown.component.html",
    styleUrls: ["./dropdown.component.scss"],
    host: {
        '(document:click)': 'handleClick($event)',
    }
})

export class DropdownComponent {

    @Input() visible: any;
    @Input() options: any;
    @Output() visibleEvent = new EventEmitter();
    @Output() selectEvent = new EventEmitter();

    constructor (private elementRef: ElementRef) {}

    handleClick(event) {
      if (!event.srcElement.classList.contains("app-dropdown")) {
        var clickedComponent = event.target;
        var inside = false;
        do {
            if (clickedComponent === this.elementRef.nativeElement) {
                inside = true;
            }
            clickedComponent = clickedComponent.parentNode;
        } while (clickedComponent);
        if(!inside){
          if (this.visible === true) {
            this.hide();
          }
        }
      }
    }

    select(option) {
      this.selectEvent.emit(option);
      this.hide();
    }

    hide() {
      this.visible = false;
      this.visibleEvent.emit(this.visible);
    }
  }
