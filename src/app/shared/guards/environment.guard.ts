import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { Subscription, Observable } from "rxjs/Rx";
import { HttpService } from "../services/http.services";
import { EnvironmentService } from "../services/environment.services";

@Injectable()
export class EnvironmentGuard implements CanActivate {

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private httpService: HttpService, private environmentService: EnvironmentService) { }

  canActivate(activatedRouteSnapshot: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    if (activatedRouteSnapshot.params.hasOwnProperty('environment')) {
      let environmentName = activatedRouteSnapshot.params.environment;
      if (!this.environmentService.environment) {
        return this.getEnvironment(environmentName);
      }
      else if (environmentName != this.environmentService.environment.name) {
        return this.getEnvironment(environmentName);
      }
      else if (environmentName == this.environmentService.environment.name) {
        return Observable.of(true);
      }
    }
    else {
      return Observable.of(false);
    }
  }

  getEnvironment(environmentName) {
    return this.httpService.getEnvironment(environmentName)
      .map((response: any) => {
        this.environmentService.setEnvironment(response);
        return true;
      })
      .catch((error:any) => {
        this.router.navigate(["oops"])
        return Observable.of(false);
      })
  }
}
