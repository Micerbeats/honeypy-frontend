import { Injectable } from '@angular/core';
import { TestService } from '../services/test.services';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, NavigationExtras, ActivatedRoute, ParamMap } from "@angular/router";
import { Subscription, Observable } from "rxjs/Rx";
import { PanelService } from "../services/panel.services";
import { SetService } from "../services/set.services";
import { HttpService } from "../services/http.services";
import 'rxjs/add/operator/switchMap';


@Injectable()
export class SetGuard implements CanActivate {

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private testService: TestService, private setService: SetService, private httpService: HttpService, private panelService: PanelService) { }

  canActivate(activatedRouteSnapshot: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    if (activatedRouteSnapshot["params"].hasOwnProperty("set")) {
      return this.httpService.getSet(activatedRouteSnapshot["params"]["set"])
        .map((response: any) => {
          this.setService.setSet(response);
          this.testService.setFile(undefined);
          this.panelService.setModalItem(undefined);
          return true;
        })
        .catch((error:any) => {
          this.router.navigate(["oops"]);
          this.panelService.showAlert("Set not found");
          console.error(error);
          return Observable.of(false);
        })
    }
    else {
      return Observable.of(false);
    }
  }
}
