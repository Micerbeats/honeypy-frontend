import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { Subscription, Observable } from "rxjs/Rx";
import { HttpService } from "../services/http.services";
import { HostService } from "../services/host.services";

@Injectable()
export class HostGuard implements CanActivate {

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private httpService: HttpService, private hostService: HostService) { }

  canActivate(activatedRouteSnapshot: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    if (activatedRouteSnapshot.params.hasOwnProperty('host')) {
      let hostId = activatedRouteSnapshot.params.host;
      if (!this.hostService.host) {
        return this.getHost(hostId);
      }
      else if (hostId != this.hostService.host.name) {
        return this.getHost(hostId);
      }
        else if (hostId == this.hostService.host.name) {
        return Observable.of(true);
      }
    }
    else {
      return Observable.of(false);
    }
  }

  getHost(hostId) {
    return this.httpService.getHost(hostId)
      .map((response: any) => {
        this.hostService.setHost(response);
        return true;
      })
      .catch((error:any) => {
        this.router.navigate(["oops"])
        return Observable.of(false);
      })
  }
}
