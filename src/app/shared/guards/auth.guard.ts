import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { Subscription, Observable } from "rxjs/Rx";
import { AuthService } from "../services/auth.services";

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private authService: AuthService) {}

  canActivate(activatedRouteSnapshot: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    if (this.authService.isAuthenticated()) {
      return Observable.of(true);
    }
    else {
      return Observable.of(false);
    }
  }
}
