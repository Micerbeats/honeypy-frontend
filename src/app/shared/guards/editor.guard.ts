import { Injectable } from '@angular/core';
import { TestService } from '../services/test.services';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, NavigationExtras, ActivatedRoute, ParamMap } from "@angular/router";
import { Subscription, Observable } from "rxjs/Rx";
import { PanelService } from "../services/panel.services";
import { HttpService } from "../services/http.services";
import 'rxjs/add/operator/switchMap';


@Injectable()
export class EditorGuard implements CanActivate {

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private testService: TestService, private httpService: HttpService, private panelService: PanelService) { }

  canActivate(activatedRouteSnapshot: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    if (activatedRouteSnapshot["queryParams"].hasOwnProperty("feature")) {
      return this.httpService.getFile("feature", activatedRouteSnapshot["queryParams"]["feature"])
        .map((response: any) => {
          this.testService.setFile(response);
          this.panelService.setModalItem(undefined);
          return true;
        })
        .catch((error:any) => {
          this.router.navigate(["oops"])
          return Observable.of(false);
        })
    }
    else if (activatedRouteSnapshot["queryParams"].hasOwnProperty("phrase")) {
      return this.httpService.getFile("phrase", activatedRouteSnapshot["queryParams"]["phrase"])
        .map((response: any) => {
          this.testService.setFile(response);
          this.panelService.setModalItem(undefined);
          return true;
        })
        .catch((error:any) => {
          this.router.navigate(["oops"])
          return Observable.of(false);
        })
    }
  }
}
