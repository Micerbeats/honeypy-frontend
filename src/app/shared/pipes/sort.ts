import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortSS'
})

export class SortPipe implements PipeTransform {

  transform(array: Array<any>, args): Array<string> {
    if (array.length > 0) {
      if (array[0].kind != "set") {
        return this.sortDirectory(array);
      }
      else {
        return this.sortArray(array);
      }
    }
  }

  sortDirectory(tree) {
    let files = [];
    let folders = [];
    let messages = [];
    for (var index in tree) {
      var item = tree[index];
      if (item.type == "file") {
        files.push(item);
      }
      else if (item.type == "folder") {
        folders.push(item);
      }
      else {
        messages.push(item);
      }
    }

    files = this.sortArray(files);
    folders = this.sortArray(folders);
    messages = this.sortArray(messages);

    return folders.concat(files).concat(messages);
  }

  sortArray(array) {
    return array.sort(
      (a, b) => {
        if (a.name > b.name) {
          return 1;
        }
        else if (a.name < b.name) {
          return -1;
        }
        else {
          return 0;
        }
      }
    );
  }
}
