import {
  Component,
  Input,
  Output,
  EventEmitter,
  ElementRef,
  OnChanges,
  ChangeDetectorRef
} from '@angular/core';

import { environment } from "../../../environments/environment";
import { DashboardService } from "../services/dashboard.services";

@Component({
  selector: 'app-checkbox-dropdown',
  templateUrl: "./checkbox-dropdown.component.html",
  styleUrls: ["./checkbox-dropdown.component.scss"],
  host: {
    '(document:click)': 'handleClick($event)',
  }
})

export class CheckboxDropdownComponent {

  visible: boolean = false;
  all: boolean;
  query: any;
  options: any;
  selections: any = [];
  @Input() label: string;

  @Input()
  set _options(options: any) {
    if (options) {
      this.options = options;
      this.initQuery();
      this.checkAll();
    }
    else {
      this.options = [];
    }
  }

  constructor(
    private dashboardService: DashboardService,
    private ref: ChangeDetectorRef,
    private elementRef: ElementRef
  ) {
    this.dashboardService.queryEvent.subscribe(
      (query) => {
        this.query = query;
      }
    );

    if (!this.dashboardService.query) {
      this.dashboardService.getQuery();
    }
    else {
      this.query = this.dashboardService.query;
      this.initQuery();
    }
  }
  initQuery() {
    if (this.options && this.query) {
      for (let option of this.options) {
        this.setSelection(option);
      }
    }
  }

  handleClick(event) {
    var clickedComponent = event.target;
    var inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if (!inside) {
      if (this.visible === true) {
        this.toggleVisibility();
      }
    }
  }

  toggleVisibility() {
    this.visible = !this.visible;
  }

  addSelection(selection) {
    let index = this.selections.indexOf(selection);
    if (index == -1) {
      this.selections.push(selection);
    }
  }

  removeSelection(selection, click:boolean = false) {
    let index = this.selections.indexOf(selection);
    if (index > -1) {
      this.selections.splice(index, 1);
      if (selection == "All") {
        this.toggleAll();
      }
      else if (click) {
        for (let option of this.options) {
          if (option.name == selection) {
            this.removeOption(option);
            this.selectionEvent(option);
            break;
          }
        }
      }
    }
  }

  removeOption(option, click:boolean = false) {
    option.toggle = false;
    this.removeSelection(option.name, click)
    this.checkAll()
  }

  addOption(option) {
    option.toggle = true;
    this.addSelection(option.name);
    this.checkAll();
  }

  toggleOption(option) {
    if (option.toggle) {
      this.removeOption(option);
    }
    else {
      this.addOption(option);
    }
    this.selectionEvent(option);
  }

  checkAll() {
    let total = 0;
    if (this.options) {
      for (let option of this.options) {
        if (!option.toggle) {
          this.removeSelection(option.name);
        }
        else {
          this.addSelection(option.name);
          total = total + 1;
        }
      }
      if ((total > 0) && (this.options.length == total)) {
        this.selections = ["All"];
        this.all = true;
      }
      else {
        let index = this.selections.indexOf("All");
        if (index > -1) {
          this.selections.splice(index, 1);
        }
        this.all = false;
      }
    }
  }

  toggleAll() {
    this.all = !this.all;
    if (this.all) {
      this.selections = ["All"];
      for (let option of this.options) {
        option.toggle = true;
        this.selectionEvent(option);
      }
    }
    else {
      this.selections = [];
      for (let option of this.options) {
        option.toggle = false;
        this.selectionEvent(option);
      }
    }
    this.checkAll();
  }

  setSelection(option) {
    let index;
    if (this.label == "Host(s)") {
      index = this.query.hosts.indexOf(option.name);
    }
    else if (this.label == "Browser(s)") {
      index = this.query.browsers.indexOf(option.name);
    }
    else if (this.label == "Environment(s)") {
      index = this.query.environments.indexOf(option.name);
    }
    if (index > -1) {
      this.addOption(option);
    }
  }

  selectionEvent(option) {
    if (this.label == "Host(s)") {
      this.setHostQuery(option);
    }
    else if (this.label == "Browser(s)") {
      this.setBrowserQuery(option);
    }
    else if (this.label == "Environment(s)") {
      this.setEnvironmentQuery(option);
    }
    this.dashboardService.setQuery(this.query);
  }

  setHostQuery(option) {
    let index = this.query.hosts.indexOf(option.name);
    if (option.toggle) {
      if (index == -1) {
        this.query.hosts.push(option.name)
      }
    }
    else {
      this.query.hosts.splice(index, 1);
    }
  }

  setBrowserQuery(option) {
    let index = this.query.browsers.indexOf(option.name);
    if (option.toggle) {
      if (index == -1) {
        this.query.browsers.push(option.name)
      }
    }
    else {
      this.query.browsers.splice(index, 1);
    }
  }

  setEnvironmentQuery(option) {
    let index = this.query.environments.indexOf(option.name);
    if (option.toggle) {
      if (index == -1) {
        this.query.environments.push(option.name)
      }
    }
    else {
      this.query.environments.splice(index, 1);
    }
  }
}
