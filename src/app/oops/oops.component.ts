import { Component } from '@angular/core';
import { HttpService } from '../shared/services/http.services';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-oops',
  templateUrl: './oops.component.html',
  styleUrls: ['./oops.component.scss']
})
export class OopsComponent {

  gif: any;

  constructor(private httpService: HttpService, public sanitizer: DomSanitizer) {
    this.httpService.getGif("oops").subscribe(
      (data) => {
        let url = data.data[Math.floor(Math.random() * data.data.length)].embed_url;
        this.gif = this.sanitizer.bypassSecurityTrustResourceUrl(url);
      },
      (error) => {}
    );
  }

}
