import { Component, Input } from '@angular/core';
import { PanelService } from '../shared/services/panel.services';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent {

  message: string;

  constructor(private panelService: PanelService) {
    if (!this.message) {
      this.message = this.panelService.panels.alert.message;
    }

    this.panelService.panelsEvent.subscribe(
      (panels) => {
        this.message = panels.alert.message;
      }
    );
  }

}
