import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PanelService } from '../shared/services/panel.services';
import { TestService } from '../shared/services/test.services';
import { AuthService } from '../shared/services/auth.services';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  reportVisibility: boolean = false;
  panels: any;
  menu: boolean = false;
  headerState: string;
  file: any;
  set: any;

  constructor(
    private panelService: PanelService,
    private testService: TestService,
    public authService: AuthService,
    private router: Router
  ) {
    if (!this.panels) {
      this.panels = this.panelService.panels;
    }

    this.panelService.panelsEvent.subscribe(
      (panels) => {
        this.panels = panels;
      }
    );

    this.testService.fileEvent.subscribe(
      (file) => {
        this.file = file;
        this.setHeader();
      }
    );
  }

  setHeader() {
    if (this.file) {
      this.headerState = "file";
    }
    else if (this.set) {
      this.headerState = "set";
    }
  }

  toggleReport() {
    if (this.panels.report.visibility === true) {
      this.panelService.closeReport();
    }
    else if (this.panels.report.visibility === false) {
      this.panelService.openReport();
    }
  }

  toggleMenu() {
    this.menu = !this.menu;
  }

  openModal(type) {
    this.panelService.openModal(type);
    this.toggleMenu();
  }

  navigateQueues() {
    let url = "";
    url = environment.rqDashboard;
    var win = window.open(url, '_blank');
    win.focus();
  }

  logout() {
    this.authService.logout();
    this.router.navigate(["login"]);
    this.panelService.toggleAuth();
  }
}
