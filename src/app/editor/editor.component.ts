import { Component, ElementRef, OnDestroy, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { TestService } from '../shared/services/test.services';
import { ReportService } from '../shared/services/report.services';
import { HttpService } from '../shared/services/http.services';
import { PanelService } from '../shared/services/panel.services';
import { HostService } from '../shared/services/host.services';

// import "codemirror/lib/codemirror";
// import 'codemirror/addon/edit/closeBrackets.js';
// import 'codemirror/addon/comment/comment.js';
import 'codemirror/keymap/sublime.js';
import '../../mode/gherkin.js';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
  host: {
    '(window:keydown)': 'hotkeys($event)',
    '(document:click)': 'handleClick($event)'
  }
})
export class EditorComponent implements OnDestroy {

  file: any;
  originalFile: any;
  editorMenu: boolean = false;
  saved: boolean = true;
  fileMenu: boolean;
  options = ["Save", "Associate to Set", "Details"];
  config;

  browsers = [];

  hosts = [];
  running: boolean = false;

  constructor(
    private testService: TestService,
    private httpService: HttpService,
    private panelService: PanelService,
    private hostService: HostService,
    private reportService: ReportService,
    private router: Router,
    private elementRef: ElementRef
  ) {

    this.running = this.reportService.running;
    this.reportService.runEvent.subscribe(
      (running) => {
        this.running = running;
      }
    );

    this.config = {
      lineNumbers: true,
      lineWrapping: true,
      autoCloseBrackets: true,
      keyMap: "sublime",
      toggleComment: true,
      lineComment: true,
      mode: "gherkin",
      tabSize: 2
    };

    this.browsers = this.testService.browsers;

    this.hostService.hostsEvent.subscribe(
      (hosts) => {this.hosts = hosts}
    );

    if (this.hosts.length === 0) {
      this.hostService.getHosts();
    }

    this.testService.fileEvent.subscribe(
      (file) => {
        if (file) {
          this.parseFile(file);
        }
      }
    );

    if (!(this.file) && (this.testService.file)) {
      let file = this.testService.file;
      this.parseFile(file);
    }

    this.reportService.runEvent.subscribe(
      (running) => {
        this.running = running;
      }
    );

  }

  parseFile(file) {
    let file_copy = Object.assign({}, file);
    let contents = this.combineContent(file_copy.contents);
    file_copy.contents = contents;
    this.originalFile = Object.assign({}, file_copy);
    this.file = file_copy;
  }

  hotkeys(event) {
    if( (event.keyCode == 83 && event.ctrlKey) || (event.keyCode == 83 && event.metaKey)) {
      event.preventDefault();
      this.saveFile();
    }
    else if ( (event.metaKey && event.keyCode == 13) || (event.ctrlKey && event.keyCode == 13) ) {
      this.run();
    }
    else if ( (event.keyCode == 8) && (event.srcElement == "textarea") ) {
      event.preventDefault();
    }
  }

  associateFeature() {
    this.panelService.openModal("associate");
    this.panelService.setModalItem(this.file);
  }

  dissociateFeature(set) {
    this.httpService.dissociateSet(set, [this.file.path]).subscribe(
      (data) => {
        this.panelService.showAlert("Feature dissociated from set");
        this.testService.fetchFile(this.file.kind, this.file.path)
      },
      (error) => {
        console.error(error.error);
        this.panelService.showAlert("Error disociating set");
      }
    );
  }

  saveFile() {
    if (this.saved === false) {
      let file = Object.assign({}, this.file);
      let contents = this.splitContent(file.contents);
      file.contents = contents;
      delete file._id;
      if (file.path.match(/(\.feature$)/i)) {
        file.type == "feature";
      }
      else if (file.path.match(/(\.phrase$)/i)) {
        file.type == "phrase";
      }
      this.testService.saveFile(file);
      this.saved = true;
      let run = Object.assign({}, file);
      return run;
    }
  }

  toggleEditorMenu() {
    this.editorMenu = !this.editorMenu;
  }

  hostChange(host) {
    this.file.host == host.name;
  }

  combineContent(content) {
    return content.join("\n");
  }

  splitContent(content) {
    return content.split(/\r?\n/);
  }

  // run test
  run() {
    let file = Object.assign({}, this.file);
    if (!this.saved) {
      file = this.saveFile();
    }
    else {
      let contents = this.splitContent(file.contents);
      file.contents = contents;
    }
    this.reportService.run(file);
  }

  ngOnDestroy() {
    this.testService.setFile(undefined);
  }

  fileChange(field, event) {
    if (this.originalFile[field] != event) {
        this.saved = false;
    }
    else {
      this.saved = true;
    }
  }

  navigateToSet(name) {
    this.router.navigate(["set", name]);
  }

  handleClick(event) {
    if (!event.srcElement.classList.contains("app-dropdown")) {
      if (this.fileMenu) {
        this.toggleDropdown();
      }
    }
  }

  toggleDropdown() {
    this.fileMenu = !this.fileMenu;
  }

  toggleDetails() {
    this.panelService.openModal("details");
  }

  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler(event) {
    event.returnValue = "Are you sure you want to exit without saving?";
  }

}
