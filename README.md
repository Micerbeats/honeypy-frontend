# Honeypy Frontend    
---

The repo contains all the code to run the frontend of Honeypy. If you want the frontend to work correctly, you will have to setup the rest of your dev environment from the other honeypy repos. The goal of these READMEs are to help you setup a full Honeypy dev environment by the end.

## Dependencies

- Node v8.9.4 & NPM v5.6.0 (https://docs.npmjs.com/getting-started/installing-node)
- Checkout the `package.json` file for details on package versions

## Setup Development Environment
- Clone the project
- Navigate to the project root
- Run `npm install` to setup the project
- Once finished, run `ng serve` to start the dev server
- The site should be accessible at `http://localhost:4200/`
